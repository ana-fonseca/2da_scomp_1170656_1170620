#include <sys/types.h>
#include <unistd.h>

int main(){

	pid_t p1, p2, p11, p21;
	int status1, status2;
	p1 = fork(); 
	
	if(p1 == -1){
		printf("Erro: Não há recursos!");
		exit(-1);
	}

	if(p1 > 0){
		
		p2 = fork();
		
		if(p2 == -1){
			printf("Erro: Não há recursos!");
			exit(-1);
		
		}
		
		p11 = wait(&status1);
		p21 = wait(&status2);
		
		if(WIFEXITED(status1) && WIFEXITED(status2)){

			printf("Status1 value: %d\n Status2 value: %d\n", status1, status2);

		}else if(p1 == 0){

			sleep(1);
			exit(1);

		}else if(p2 == 0){

			sleep(2);
			exit(2);

		}
		
return 0;

}
