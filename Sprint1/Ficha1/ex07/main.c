#define ARRAY_SIZE 1000
#include <sys/types.h>
#include <unistd.h>


int main(){

	int numbers[ARRAY_SIZE];		/* array to lookup */
	int n;							/* the number to find */
	time_t t;						/* needed to init. the random number generator (RNG)*/
	pid_t p1, p11;					/* child PID */
	int k,j, status;
	int counter1 = 0, counter2 = 0;
	
	int i;
	
	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time (&t));
	
	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < ARRAY_SIZE; i++){
		numbers[i] = rand () % 10000;
	}
	
	/* initialize n */
	n = rand () % 10000;
	
	p1 = fork();
	
	//Garantir que o fork não deu erro
	if(p1 == -1){
		printf("Erro: Não existem recursos suficientes!");
		exit(-1);
	}
	
	//Neste caso, decidimos fazer a divisão do array percorrendo-o:
	//Pares e ímpares
	if(p1 == 0){
		
		for(k = 0; k < ARRAY_SIZE; k+2){
			if(numbers[k] == n){
				counter1++;
			}
		}
		
	}else{
		
		for(j = 1; j < ARRAY_SIZE;; i+2){
			if(numbers[j] == n){
				counter2++;
			}
		}	
		
		//Garantir que o processo filho já terminou
		p11 = waitpid (p11, &status, 0);
		
		if(WIFEXITED(status)){
			printf("Número de vezes que foi encontrado: %d", counter1 + counter2);
		}			
	}

return 0;	
}
