#include <sys/type.h>
#include <unistd.h>

//Assumimos que deve esperar pelo processo dos filhos individualmente e antes de fazer o fork(), garantimos
//que imprime pela ordem desejada.

int main(){

	printf("I'm...\n");
	int status, status1;
	pid_t p1, p2, p3, p11, p21;
	p1 = fork();

	if(p1 == -1){
		printf("Erro: Não há recursos!");
		exit(-1);
		
	}else if(p1==0){
		
		printf("I'll never join you!");
	
	}else{
		p11 = waitpid(p1, &status, 0);
		
		if(WIFEXITED(status){
			printf("the...\n");
			p2 = fork();
			
			if(p2 == -1){
				printf("Erro: Não há recursos!");
				exit(-1);
		
			}else if(p2 == 0){
			
				printf("I'll never join you!\n");
			
			}else{
				p21 = waitpid(p2, &status1, 0);
				if(WIFEXITED(status1)){
					printf("father!\n");
					p3 = fork();
				}
			
				if(p3 == 0){
					printf("I'll never join you\n");
				}
			}
		}
	
	}

return 0;

}
