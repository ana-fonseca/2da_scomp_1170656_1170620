#include <stdio.h>
#include <unistd.h>	

int main () 
{
    int i, contEven=0;;
    int status;
	pid_t p, pLista[NUM_ELEM];

    for (i = 0; i < 4; i++)
    {
		p = fork ();
        if (p == 0) {
            sleep (1);		/*sleep(): unistd.h */
			exit(0);  
        } else if( (p > 0) && (p % 2 == 0) ){ /* ser o processo pai e o filho ter pid par ( % 2 == 0) */
			
			pLista[contEven]= p;
			contEven++;
			
		} else if(p == -1){
		
			printf("Erro: Não há recursos!");
			exit(-1);
		}
		
    }
	
	for( i = 0;i < contEven:i++ ){
	
		waitpid(pLista[i],&status,0);
	
	}
	
    printf ("This is the end.\n");
	return 0;
}
