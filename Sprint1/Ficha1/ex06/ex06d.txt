#include <stdio.h>
#include <unistd.h>	
#define NUM_ELEM 4

int main () 
{
	
    int i, cont = 0, contEven=0;
    int status;
	pid_t p, pLista[NUM_ELEM];

    for (i = 0; i < NUM_ELEM; i++)
    {
		p = fork ();
        if (p == 0) {
			
            sleep (1);		/*sleep(): unistd.h */
			exit(i + 1);  /* ex: no caso de i = 0, como é primeiro vai retonar 0 + 1 = 1 */
        } else if( (p > 0) && (p % 2 == 0) ){ /* ser o processo pai e o filho ter pid par ( % 2 == 0) */
			
			pLista[contEven]= p;
			contEven++;
			
		} else if(p == -1){
		
			printf("Erro: Não há recursos!");
			exit(-1);
		
		}
		
    }
	
	for( i = 0;i < contEven:i++ ){
	
		waitpid(pLista[i],&status,0);
	
	}
	
    printf ("This is the end.\n");
	return 0;
}
