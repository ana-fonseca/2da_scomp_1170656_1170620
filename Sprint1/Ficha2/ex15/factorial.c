#include <unistd.h>

int main(void){
	int num, i, fact = 1;
	
	/* vai estar à espera ate receber um numero */
	read(0, &num, sizeof(num));

	//fazer fatorial
	for (i = 1; i <= num; i++){
		
		fact *= i;
	}
	/* escreve dados no descritor que está na posição 1 da tabela */
	write(1, &fact, sizeof(int));
	
	return 0;
}
