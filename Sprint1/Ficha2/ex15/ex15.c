#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SIZE 2

int main(void) {
	int i, fd[SIZE][2];
	int num, factorial;	
	
	pid_t p;
	
	for(i = 0; i < SIZE; i++) {
		if(pipe(fd[i]) == -1) {
			printf("Pipe failed\n");
			return 0;
		}
	}

	p=fork();
	if( p == 0) {	
		/* fecha a extremidade não usada */
		close(fd[0][1]); //no pipe 0 fechar o de escrita
		close(fd[1][0]); //no pipe 1 fechar o de leitura

		/* Altera os descritores */
		dup2(fd[0][0], 0); //o descritor 0  deixa de ser stdin, mas o descritor de leitura do pipe 0 
		dup2(fd[1][1], 1); //o descritor 1 deixa de ser stdout, mas o descritor de escrita do pipe 1

		execl("./factorial", "./factorial", NULL); //correr o codigo no ficheiro fatorial(mesmo dir que o atual)

		//se o execl for bem executado, as proximas linhas nao vao ser corridas
		close(fd[0][0]);
		close(fd[1][1]);

		exit(1);

	} else if( p = -1){
		 printf("Erro fork, não há recursos\n");

	} else {	
		printf("Qual é o seu numero? ");

		scanf("%d", &num); // numero para fatorial
		
		/* fecha a extremidade não usada */
		close(fd[0][0]);
		close(fd[1][1]);

		/* escreve dados do pipe */
		write(fd[0][1], &num, sizeof(int));

		/* fecha a extremidade de escrita */ 
		close(fd[0][1]);

		/* lê no pipe */ 
		read(fd[1][0], &factorial, sizeof(int));

		/* fecha a extremidade de leitura */ 
		close(fd[1][0]);
		printf("Factorial: %d\n", factorial);

	}
	return 0;
}
