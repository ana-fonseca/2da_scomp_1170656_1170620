#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>

#define INITIAL_CREDIT 20

int main(){
	
	time_t tm;
	int balance = INITIAL_CREDIT;
	int descriptor[2][2];
	int i, can_bet = 1;
	pid_t pid;
	
	for(i = 0; i < 2; i++) {
		if(pipe(descriptor[i]) == -1) {
			printf("Error creating pipe!");
			exit(0);
		}
	}
	
	pid = fork();
	
	if(pid == -1) {
		
		printf("Error: Creating child process!");
		exit(0);
		
	} else if(pid == 0) {
		
		// Initializes RNG
		srand ((unsigned) time (&tm) - getpid());

		read(descriptor[0][0], &can_bet, sizeof(int));
		
		while(can_bet == 1) {
			
			int random = 1 + (rand() % 5);
			
			write(descriptor[1][1], &random, sizeof(int));
			read(descriptor[0][0], &balance, sizeof(balance));
			
			printf("Balance: %d\n", balance);
			
			read(descriptor[0][0], &can_bet, sizeof(int));
			
		}
		
		close(descriptor[0][0]);
		close(descriptor[1][1]);
		exit(0);
		
	} else {
		
		srand ((unsigned) time (&tm) - getpid());
		
		while(balance > 0) {
			
			int random = 1 + (rand() % 5);
			int bet_num;
			
			write(descriptor[0][1], &can_bet, sizeof(int));
			read(descriptor[1][0], &bet_num, sizeof(int));
			
			//+10 se ganhar -5 se perder
			balance += (random == bet_num ? 10 : -5);
			
			write(descriptor[0][1], &balance, sizeof(int));
		}
		
		can_bet = 0;
		write(descriptor[0][1], &can_bet, sizeof(int));
		
	}
	
	return 0;
}
