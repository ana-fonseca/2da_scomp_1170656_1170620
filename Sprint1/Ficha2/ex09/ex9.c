#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "saleRecord.h"

#define SIZE 50000
#define NUMBER_PIPES 10
#define MIN_QUANT 20


int main(void){
	pid_t pid;
	
	saleRecord vecSales[SIZE];
	int vecProductsCode[SIZE], products[SIZE];
	int fd[NUMBER_PIPES][2],*ptrPipe;
	int elem, c, c2, c3, status, i, i2, i3;
	
	pid_t vecPids[NUMBER_PIPES];

	for(i = 0 ; i < NUMBER_PIPES; i++){
		if(pipe(fd[i]) == -1){
			perror("Pipe failed");
			return 1;
		}	
	}
	vecSales[0].customer_code=123;
	vecSales[0].product_code=111;
	vecSales[0].quantity=24;
	
	for(i2 = 0; i2 < NUMBER_PIPES ; i2++){
	
		pid = fork();
		if(pid > 0){ 
			vecPids[i2]=pid;			 
		} else if(pid == 0){
			
			ptrPipe=fd[i2];
			
			/* fecha a extremidade não usada */ 
			close(ptrPipe[0]);
					 
			
			for(c = 0 ; c < (SIZE/NUMBER_PIPES); c++){
				elem=(SIZE/NUMBER_PIPES)*i+c;
				if(vecSales[elem].quantity >= MIN_QUANT){		
					/* escreve no pipe */ 
					write(ptrPipe[1],&vecSales[elem].product_code,sizeof(int) ); 			
				}
			}	 
			/* fecha a extremidade de escrita */ 
			close(ptrPipe[1]); 
			exit(0);
			
		} else{
			printf("Erro fork, não há recursos");
		}
			
	}
	

	for(c2 = 0; c2 < NUMBER_PIPES; c2++){
		
		waitpid(vecPids[c2], &status,0);
		
	}
	
	int index;
	for(c3 = 0; c3 < NUMBER_PIPES; c3++){
		
		ptrPipe=fd[c3];
		
		/* fecha a extremidade não usada */
		close(ptrPipe[1]); 
		
		/* lê dados do pipe */
		read(ptrPipe[0], vecProductsCode, sizeof(int)*(SIZE/NUMBER_PIPES)); 

		/* fecha a extremidade de leitura */ 
		close(ptrPipe[0]);
		
		for(i3 = 0; i3 < SIZE/NUMBER_PIPES;i3++){
			index=i3+((SIZE/NUMBER_PIPES)*c3);
			products[index] = vecProductsCode[i3];

		}
	}

	return 0;
}
