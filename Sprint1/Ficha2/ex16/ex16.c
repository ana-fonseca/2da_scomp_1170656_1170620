#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

#define PROCESSES 3

int main(void){
	
	int descriptor[PROCESSES - 1][2], i;
	pid_t pid;
	
	for (i = 0; i < PROCESSES - 1; i++){
		if (pipe(descriptor[i]) == -1){
			printf("Error creating pipe.");
			exit(0);
		}
	}
	
	for (i = 0; i < PROCESSES; i++){
		pid = fork();
		if ((pid) == -1){
			printf("Error creating child process %d.", i + 1);
			exit(0);
		} 
		
		if (pid == 0){
			if (i == 0){
				
				close(descriptor[1][1]);
				close(descriptor[1][0]);
				close(descriptor[0][0]);
				
				dup2(descriptor[0][1], 1);
				
				close(descriptor[0][1]);
				
				execlp("ls", "ls", "-la", NULL);
			
				exit(-1);
				
			} else if (i == 1){
				
				close(descriptor[1][0]);
				close(descriptor[0][1]);
				dup2(descriptor[0][0], 0);
				dup2(descriptor[1][1], 1);
				close(descriptor[1][1]);
				close(descriptor[0][0]);
				
				
				execlp("sort", "sort", NULL);
				exit(-1);
				
			} else if (i == 2){
				close(descriptor[0][1]);
				close(descriptor[0][0]);
				close(descriptor[1][1]);
				
				dup2(descriptor[1][0], 0);
				
				close(descriptor[1][0]);
				
				execlp("wc", "wc", "-l", NULL);
				exit(-1);
			}
		}
	}
	wait(NULL);
	
	return 0;
}
