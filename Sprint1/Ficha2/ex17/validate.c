#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define INVALID -1
#define VALID 0
#define PASS_INV 1
#define USER_INV 2
#define SIZE_PASS 30

int main(void) {
		
	int stat = INVALID;
	//vao ser recebidas
	char pass[SIZE_PASS];
	int id;
	
	//criadas para testar
	char user_pass[SIZE_PASS];
	int user_id = 17;
	
	strcpy(user_pass, "userPass");
	
	read(0, &pass, SIZE_PASS);	
	
	read(0, &id, sizeof(int));
	
	if(user_id != id){
		stat = USER_INV;
	
	}else if(strcmp(pass, user_pass) != 0){
		stat = PASS_INV;
	}
	else{
		stat = VALID;
	}
	
	write(1, &stat, sizeof(int));
	
	return 0;
}
