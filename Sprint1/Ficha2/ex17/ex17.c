#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SIZE 2 //pipes

#define VALID 0
#define INVALID_PASS 1
#define INVALID_USER 2

int main(void) {
	char pass[20];
	
	int j, id, fd[SIZE][2];
	
	for(j = 0; j < SIZE; j++) {
		if(pipe(fd[j]) == -1) {
			
			printf("Error creating pipe\n");
			return 0;
		}
	}
	
	printf("Your password: ");
	scanf("%s", pass);
	
	printf("\nYour ID: ");
	scanf("%d", &id);
	
	if(fork() == 0) {
		close(fd[0][1]);
		close(fd[1][0]);
		
		dup2(fd[0][0], 0);
		dup2(fd[1][1], 1);
		
		execl("./validate", "./validate", NULL);
		
		close(fd[0][0]);
		close(fd[1][1]);
		
		exit(0);
	} else {
		int res;
		
		close(fd[0][0]);
		close(fd[1][1]);
		
		write(fd[0][1], &id, sizeof(int));
		write(fd[0][1], &pass, 20);
		
		close(fd[0][1]);
		
		read(fd[1][0], &res, sizeof(int));
		
		close(fd[1][0]);
		
		if(res == INVALID_USER){
			printf("O user não foi encontrado\n");
		
		}else if(res == INVALID_PASS){
			printf("Password não válida\n");
			
		}else{
			printf("Login válido\n");
		}
	}
	
	return 0;
}
