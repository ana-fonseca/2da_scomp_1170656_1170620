#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void){
	pid_t pid,pid2;
	
	int fd[2];
	
	if(pipe(fd) == -1){
			
		perror("Pipe failed");
		return 1;
	}
	 
	pid = fork();
	if(pid > 0){ 
		 /* fecha a extremidade não usada */ 
		 close(fd[0]);
		 
		 printf("Son's pid %d: ",(int)pid);
		 
		 /* escreve no pipe */ 
		 write(fd[1],&pid, sizeof(pid_t)); 
		 /* fecha a extremidade de escrita */ 
		 close(fd[1]); 
	} else if(pid == 0){
		
		/* fecha a extremidade não usada */
		close(fd[1]); 
		
		/* lê dados do pipe */
		pid2=0;
		read(fd[0], &pid2, sizeof(pid_t)); 
		printf("Filho leu: %d", (int)pid2); 
		/* fecha a extremidade de leitura */ 
		close(fd[0]); 
	
	} else{
		printf("Erro fork, não há recursos");
	}

	return 0;
}
