#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#define MAX_CHILDREN 10
#define MAX_ROUNDS 10

typedef struct{
	int round;
	char* message;
} Game;

int main(){

int descriptor[2], pid_keeper[MAX_CHILDREN];
int child, pid_status;
Game rnd;

if(pipe(descriptor) == -1){
	printf("ERROR: Creating pipe\n");
	exit(0);
}

for(child = 0; child < MAX_CHILDREN; child++){
	pid_t pid = fork();
	if(pid == -1){
		
		printf("ERROR: Creating child proccess\n");
		exit(0);
	
	//CHILD	
	}else if(pid == 0){
		
		close(descriptor[1]);
		read(descriptor[0], &rnd, sizeof(rnd));
		close(descriptor[1]);
		
		printf("PID: %d | [i = %d] | Round number %d won!\n", getpid(), child, rnd.round);
		exit(rnd.round);
		
	}else{
		
		pid_keeper[child] = pid;
	
	}
}
	
	close(descriptor[0]);
	rnd.message = "WIN!";
	
	for(child = 0; child < MAX_ROUNDS; child++){
		sleep(2);
		rnd.round = child + 1;
		write(descriptor[1], &rnd, sizeof(rnd));
	}
	
	close(descriptor[1]);
	
	for(child = 0; child < MAX_CHILDREN; child++){
		waitpid(pid_keeper[child], &pid_status, 0);
		if(WIFEXITED(pid_status)){
			printf("Child: %d won round %d!\n", pid_keeper[child], WEXITSTATUS(pid_status));
		}
	}			

return 0;
}
