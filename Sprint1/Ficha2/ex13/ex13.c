#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define SIZE 200

int main(void) {
	int fd[2], status;
	
	pid_t p;
	
	if(pipe(fd) == -1) {
		printf("Pipe failed\n");
		return 0;
	}
	close(fd[1]);

	p=fork();
	if(p == 0) {
		close(fd[0]);

		//altera o descritor 1, que está associado ao standart output para o descritor de escrita do fd
		dup2(fd[1], 1);
		execl("/bin/sort", "sort", "fx.txt", NULL);

		close(fd[1]);

		exit(1);
	} else if( p == -1){
		printf("Erro fork, não há recursos");
		
		exit(1);
	}

	int index;
	char str[SIZE];
	while((index = read(fd[0], str, SIZE)) != 0){
		printf("%s\n", str);

	}

	wait(status);
	
	//fechar o descritor de leitura
	close(fd[0]);

	return 0;
}
