#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define MAX_CHILDREN 5
#define SIZE 1000

int main(){

pid_t pid; 
int vec1[SIZE];
int vec2[SIZE];
int results[SIZE];
int children, p;
int fill, i;
time_t t;
int fd[2];

//Filling vecs using rand
srand ((unsigned) time(&t));
for (fill = 0; fill < SIZE; fill++){
	vec1[fill] = rand() % 100;
	vec2[fill] = rand() % 100;
}

if(pipe(fd) == -1) {
		printf("Nao foi possivel criar pipe.");
		exit(0);
}

//Creating children processes and adding the numbers;
for (children = 0; children < MAX_CHILDREN; children++){

	pid = fork();
	
	// Catch error from fork()
	if(pid == -1){
		fprintf(stderr, "%.5d: failed to fork (%d: %s)\n",
                (int)getpid(), errno, strerror(errno));
		exit(-1);
		
	//CHILDREN
	}else if (pid == 0){
		
			int init_num = children * (SIZE / MAX_CHILDREN);
			int final_num = (children + 1) * (SIZE / MAX_CHILDREN);
			
			int tmp[SIZE/MAX_CHILDREN];
			int k = 0;
			
			for(p = init_num; p < final_num; p++) {
				tmp[k] = vec1[p] + vec2[p];
				k++;
			}
			
			if(write(fd[1], &tmp, (SIZE / MAX_CHILDREN) * sizeof(int)) == -1) {
				printf("ERROR couldn't write on pipe %d\n", children);
				exit(0);
			}
			
			
			exit(0);
		
	}
}

//READING from temporary array to final array
	for(i = 0; i < MAX_CHILDREN; i++) {		
		
		
		int tmp[SIZE/MAX_CHILDREN];
		if(read(fd[0], &tmp, (SIZE/MAX_CHILDREN) * sizeof(int)) == -1) {
			printf("ERROR Reading from pipe %d\n", i);
			exit(0);
		}
		
		
		int init_num = i * (SIZE / MAX_CHILDREN);
		int final_num = (i + 1) * (SIZE / MAX_CHILDREN);
		int k = 0;
		
		for(p = init_num; p < final_num; p++) {
			results[p] = tmp[k];
			k++;
		}
	}

printf("SHOW: \n");
//first five
	for(p = 0; p < MAX_CHILDREN; p++) {
		for(i = 0; i < 5; i++) {
			int pos = p * (SIZE/MAX_CHILDREN) + i;
			printf("vec_1[%d]: %d | vec_2[%d]: %d | result[%d]: %d\n", pos, vec1[pos], pos, vec2[pos], pos, results[pos]);
		}
	}
	
		
return 0;	
}
	


