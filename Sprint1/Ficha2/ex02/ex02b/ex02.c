#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#define BUFFER_SIZE 90

typedef struct{
	int value;
	char str[BUFFER_SIZE];
} info;

int main(){

	pid_t pid;
	int fd[2];
	info wrt;
	
	//Creates communication channel
	if(pipe(fd) == -1){
			
		perror("Pipe failed");
		return 1;
	}
	
	pid = fork();
	if (pid > 0){
				
		printf("Insira uma string:\n");
		fgets(wrt.str, 90, stdin); //Reads from stdin
					
		printf("Insira um integer:\n");
		scanf("%d", &wrt.value);
				
		//Closes unused descriptor (read)
		close(fd[0]);		
		
		int wrt1 = write(fd[1],&wrt,sizeof(info)); 
		if(wrt1 == -1){
			printf("Erro a escrever o int value no pipe");
			exit(0);
		}
		
		close(fd[1]);
	
	}else if(pid == 0){
				
		//Closes unnused descriptor (write)
		close(fd[1]);
				
		//Read value
		int rd1 = read(fd[0], &wrt, sizeof(info)); 
	
		printf("\nInteger: %d",wrt.value);
		printf("\nString: %s", wrt.str);
		
		if(rd1 == -1){
			printf("Erro a ler a struct no pipe");
			exit(0);
		}
		
		/*Closes writting descriptor*/ 
		close(fd[0]);
		exit(0); 
		
	}else if (pid == -1){
	
		printf("Erro: Não há recursos!");
		exit(-1);
		
	}


return 0;
}
