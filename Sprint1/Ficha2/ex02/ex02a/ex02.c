#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#define BUFFER_SIZE 90

int main(){

	pid_t pid;
	int fd[2], value1; 
	char str1[BUFFER_SIZE];
	
	//Creates communication channel
	if(pipe(fd) == -1){
			
		perror("Pipe failed");
		return 1;
	}
	
	pid = fork();
	if (pid > 0){
		
		printf("Insira uma string:\n");
		fgets(str1, 90, stdin); //Reads from stdin
					
		printf("Insira um integer:\n");
		scanf("%d", &value1);
				
		//Closes unused descriptor (read)
		close(fd[0]);		
		
		int wrt1 = write(fd[1],&value1,sizeof(int)); 
		if(wrt1 == -1){
			printf("Erro a escrever o int value no pipe");
			exit(0);
		}
		
		int wrt2 = write(fd[1],&str1, strlen(str1)+1); 
		if(wrt2 == -1){
			printf("Erro a escrever o str no pipe");
			exit(0);
		}
		
		close(fd[1]);
	
	}else if(pid == 0){
				
		//Closes unnused descriptor (write)
		close(fd[1]);
				
		//Read value
		int rd1 = read(fd[0], &value1, sizeof(int)); 
		
		//Read str
		int rd2 = read(fd[0], &str1, strlen(str1)+1);
		
		printf("\nInteger: %d",value1);
		
		if(rd1 == -1){
			printf("Erro a ler o int value no pipe");
			exit(0);
		}
		
		printf("\nString: %s\n", str1);
		
		if(rd2 == -1){
			printf("Erro a ler a str no pipe");
			exit(0);
		}
		
		/*Closes writting descriptor*/ 
		close(fd[0]);
		exit(0); 
		
	}else if (pid == -1){
	
		printf("Erro: Não há recursos!");
		exit(-1);
		
	}


return 0;
}
