#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define SIZE 256

int main(void){
	pid_t pid;
	
	char vec[SIZE];
	
	int fd[2], fd2[2];
	int i,length=0;	
	
	//down
	if(pipe(fd) == -1){
			
		perror("Pipe failed");
		return 1;
	}
	//up
	if(pipe(fd2) == -1){
			
		perror("Pipe failed 2");
		return 1;
	}
	 
	pid = fork();
	if(pid > 0){ 
		
		/* fecha a extremidade não usada */
		close(fd2[1]); 
		
		/* lê dados do pipe */
		read(fd2[0], vec, sizeof(char)*SIZE); 

		/* fecha a extremidade de leitura */ 
		close(fd2[0]); 
		

		for(i=0;vec[i]!='\0';i++)
		length+=1;
		
		for(i=0;i<length;i++){
			if ((vec[i] >= 65) && (vec[i] <= 90)){
				vec[i]+=32;
			}else if((vec[i] >= 97) && (vec[i] <= 122)){
				vec[i]-=32;
			}
		}

		
		 /* fecha a extremidade não usada */ 
		 close(fd[0]);
		 
		 /* escreve no pipe */ 
		 write(fd[1],vec,sizeof(char)*SIZE ); 
		 
		 /* fecha a extremidade de escrita */ 
		 close(fd[1]); 		 
		 
	} else if(pid == 0){
		
		printf("frase: ");
		scanf("%s",vec);
		
		 /* fecha a extremidade não usada */ 
		 close(fd2[0]);
		 
		 /* escreve no pipe */ 
		 write(fd2[1],vec,sizeof(char)*SIZE ); 
		 
		 /* fecha a extremidade de escrita */ 
		 close(fd2[1]); 
		
		
		
		/* fecha a extremidade não usada */
		close(fd[1]); 
		
		/* lê dados do pipe */
		read(fd[0], vec, sizeof(char)*SIZE); 
		/* fecha a extremidade de leitura */ 
		close(fd[0]); 
		
		printf("Msg : %s",vec);
	
	} else{
		printf("Erro fork, não há recursos");
	}

	return 0;
}
