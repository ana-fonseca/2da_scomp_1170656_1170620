#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#define BUFFER_SIZE 81

int main(){
	int descriptor[2];
	pid_t p;
	
	if(pipe(descriptor) == -1) {
		printf("ERROR: Creating pipe!\n");
		exit(0);
	}
	
	p = fork();
	if(p == -1) {
		printf("ERROR: Creating child process!\n");
		exit(0);
	}
	
	if(p != 0){
		close(descriptor[0]);
		
		char str1[BUFFER_SIZE];
		int size1;
		
		FILE *fl = fopen("texto.txt", "r");
		
		if(fl == NULL)
			printf("ERROR: Opening file!\n");
		
		else{
			while(!feof(fl)){
				fgets(str1, BUFFER_SIZE, fl);
				
				size1 = strlen(str1) + 1;
				
				int res = write(descriptor[1], str1, size1);
				
				if(res == -1) {
					printf("ERROR: Writting in pipe!\n");
					exit(0);
				}
			}
		}
		
		fclose(fl);
		
		close(descriptor[1]);
		
		wait(NULL);
	}else{
		close(descriptor[1]);
		
		char str2[BUFFER_SIZE];
				
		read(descriptor[0], str2, sizeof(str2));
		printf("%s", string2);
		close(descriptor[0]);
		
		
	}
	
	return 0;
}
