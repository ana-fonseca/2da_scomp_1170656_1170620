#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define BARCODE_READERS 5
#define MAX_PRODUCT 5

typedef struct{
	char* name;
	int id;
	int price;
	int reader;
}	Product;

int main(void) {
	int fd[2];
	int descriptors[BARCODE_READERS][2];

	int i;
	Product product;
	
	Product productlist[MAX_PRODUCT];
	for(i = 0; i < MAX_PRODUCT; i++) {
		productlist[i].id = i;
		productlist[i].price = i * 4 + 3 / 2 - 1;
		productlist[i].name = "product x";
	}
	
	if(pipe(fd) == -1) {
		printf("Erro a criar pipe partilhado!");
		return 0;
	}
	
	for(i = 0; i < BARCODE_READERS; i++) {
		if(pipe(descriptors[i]) == -1) {
			printf("Erro a criar pipe para filho!");
			return 0;
		}
	}
	
	for(i = 0; i < BARCODE_READERS; i++) {
		if(fork() == 0) {
			close(fd[0]);
			close(descriptors[i][1]);
			
			scanf("%d", &product.id);
			product.reader = i;
			
			write(fd[1], &product, sizeof(product));
			close(fd[1]);
			
			read(descriptors[i][0], &product, sizeof(product));
			close(descriptors[i][0]);
			
			printf("id: %d | name: %s | price: %d\n", product.id, product.name, product.price);
			
			exit(0);
		}
	}
	
	close(fd[1]);
	
	for(i = 0; i < BARCODE_READERS; i++)
		close(descriptors[i][0]);

	
	while(read(fd[0], &product, sizeof(product)) > 0) {
		for(i = 0; i < MAX_PRODUCT; i++) {
			if(productlist[i].id == product.id) {
				productlist[i].reader = product.reader;
				write(descriptors[product.reader][1], &productlist[i], sizeof(productlist[i]));
				close(descriptors[product.reader][1]);
			}
		}
	}
	
	close(fd[0]);
	
	return 0;
}
