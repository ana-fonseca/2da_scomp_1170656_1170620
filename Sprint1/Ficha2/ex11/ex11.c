#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h> 
#include <time.h>
#include <wait.h>

#define NUMBER_PIPES (5+1)// +1, pois o pai precisa de ligar ao filho 1 e o filho 5 ao pai
#define RANDOM_LIMIT 500

int main(void){
	pid_t pid;
	
	int fd[NUMBER_PIPES][2];
	int i, j, i2, status=0;
	int numero, max=0;
	
	pid_t vecPids[NUMBER_PIPES];

	for(i = 0 ; i < NUMBER_PIPES; i++){ 
		if(pipe(fd[i]) == -1){
			perror("Pipe failed");
			return 1;
		}
	}
	
	numero = rand() % RANDOM_LIMIT;	 
	
	for(i2 = 0; i2 < NUMBER_PIPES -1 ; i2++){
	
		pid = fork();
			
		srand(getpid()^100);
		
		if(pid > 0){ 
			vecPids[i2]=pid;	
			
			//escreve para filho 1
			/* fecha a extremidade não usada */ 
			close(fd[0][0]);
				
			/* escreve no pipe */ 
			write(fd[0][1], &numero, sizeof(int) ); 			
							
			/* fecha a extremidade de escrita */ 
			close(fd[0][1]); 			

		} else if(pid == 0){
			
			/* fecha a extremidade não usada */
			close(fd[i2][1]); 
			/* lê dados do pipe */
			read(fd[i2][0], &max, sizeof(int)); 
			/* fecha a extremidade de leitura */ 
			close(fd[i2][0]);
			
			numero = rand() % RANDOM_LIMIT ;
			
			if(numero > max){
				max = numero;
			}	 
			
			printf("max: %d | pid: %d\n", max, getpid());
			
			/* fecha a extremidade não usada */ 
			close(fd[i2+1][0]);
			/* escreve no pipe */ 
			write(fd[i2+1][1],&max,sizeof(int) ); 			
			/* fecha a extremidade de escrita */ 
			close(fd[i2+1][1]); 
			
			exit(0);
			
		} else{
			printf("Erro fork, não há recursos");
		}
	}
	
	for(j = 0; j < NUMBER_PIPES; j++){
		
		waitpid(vecPids[j], &status, 0);
		
	}
	
	numero = rand() % RANDOM_LIMIT ;
	
	/* fecha a extremidade não usada */
	close(fd[5][1]); 
	/* lê dados do pipe */
	read(fd[5][0], &max, sizeof(int)); 
	/* fecha a extremidade de leitura */ 
	close(fd[5][0]);

	if(numero > max){
		max = numero;
	}	 
				
	printf("max : %d",max);

	return 0;
}
