#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#define SIZE 1000
#define NUMBER_PIPES 5


int main(void){
	pid_t pid;
	
	int vec1[SIZE], vec2[SIZE], vec3[SIZE];
	time_t t;						/* needed to init. the random number generator (RNG)*/
	int  fd1[2], fd2[2], fd3[2], fd4[2], fd5[2];
	int * fd[NUMBER_PIPES],*ptrPipe;
	int elem, c, c2, c3, status, i, i2, i3, j;
	
	pid_t vecPids[NUMBER_PIPES];
	
	fd[0]=fd1;
	fd[1]=fd2;
	fd[2]=fd3;
	fd[3]=fd4;
	fd[4]=fd5;
	
	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time (&t));
	
	/* initialize array with random numbers (rand(): stdlib.h) */
	for (j = 0; j < SIZE; j++){
		vec1[j] = rand () % 10000;
		vec2[j] = rand () % 10000;
	}
	
	for(i = 0 ; i < NUMBER_PIPES; i++){
		if(pipe(fd[i]) == -1){
			perror("Pipe failed");
			return 1;
		}	
	}
	
	for(i2 = 0; i2 < NUMBER_PIPES ; i2++){
	
		pid = fork();
		if(pid > 0){ 
			vecPids[i2]=pid;
			 
			 
		} else if(pid == 0){
			
			for(c = 0 ; c < (SIZE/NUMBER_PIPES); c++){
				elem=(SIZE/NUMBER_PIPES)*i+c;
				vec3[c]=vec1[elem]+vec2[elem];
			}
			
			ptrPipe=fd[i2];
			
			/* fecha a extremidade não usada */ 
			close(ptrPipe[0]);
					 
			/* escreve no pipe */ 
			write(ptrPipe[1],vec3,sizeof(vec3) ); 					 
					 
			/* fecha a extremidade de escrita */ 
			close(ptrPipe[1]); 
			
			exit(0);
			
		} else{
			printf("Erro fork, não há recursos");
		}
			
	}
	

	for(c2 = 0; c2 < NUMBER_PIPES; c2++){
		
		waitpid(vecPids[c2], &status,0);
		
	}
	
	int index;
	for(c3 = 0; c3 < NUMBER_PIPES; c3++){
		
		ptrPipe=fd[c3];
		
		/* fecha a extremidade não usada */
		close(ptrPipe[1]); 
		
		/* lê dados do pipe */
		read(ptrPipe[0], vec3, sizeof(vec3)); 

		/* fecha a extremidade de leitura */ 
		close(ptrPipe[0]); 
		
		for(i3 = 0; i3 < SIZE/NUMBER_PIPES;i3++){
			index=i3+((SIZE/NUMBER_PIPES)*c3);
			vec1[index] = vec3[i3];
			printf("- %d -",vec1[index]);
		}
	}

	return 0;
}
