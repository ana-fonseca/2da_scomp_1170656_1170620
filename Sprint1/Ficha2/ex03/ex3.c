#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define SIZE 30

int main(void){
	pid_t pid,pid2;
	
	char vec[SIZE]="Hello world", vec2[SIZE]="Goodbye!";
	
	int fd[2], status;
	
	if(pipe(fd) == -1){
			
		perror("Pipe failed");
		return 1;
	}
	 
	pid = fork();
	if(pid > 0){ 
		 /* fecha a extremidade não usada */ 
		 close(fd[0]);
		 
		 /* escreve no pipe */ 
		 write(fd[1],vec,sizeof(char)*SIZE ); 
		 write(fd[1],vec2,sizeof(char)*SIZE ); 
		 
		 
		 /* fecha a extremidade de escrita */ 
		 close(fd[1]); 
		 
		 pid2 = wait(&status);
		 if(WIFEXITED(status)){
			printf("Son's pid: %d\nSon's exit status %d\n",(int)pid, WEXITSTATUS(status));
		 }
		 
		 
	} else if(pid == 0){
		
		/* fecha a extremidade não usada */
		close(fd[1]); 
		
		/* lê dados do pipe */
		read(fd[0], vec, sizeof(char)*SIZE); 
		printf("\n1: %s",vec);
		read(fd[0], vec2, sizeof(char)*SIZE);
		printf("\n2: %s\n",vec2);
		/* fecha a extremidade de leitura */ 
		close(fd[0]); 
	
	} else{
		printf("Erro fork, não há recursos");
	}

	return 0;
}
