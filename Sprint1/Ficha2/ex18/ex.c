 #include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
 
 
 int main(){
	 
 int i, status, error, fd[2];
 pid_t pid;
 char message[100], read[100];

 pipe(fd);
 dup2(fd[0],0);
 dup2(fd[1],1);
 close(fd[0]);
 close(fd[1]);

 for(i=1;i<5;i++){
 pipe(fd);
 pid = fork();
 if(pid > 0)
 error = dup2(fd[1], 1);
 else
 error = dup2(fd[0], 0);

 close(fd[0]);
 close(fd[1]);

 if(pid)
 break;
 }

 sprintf(message,"This is process %d with PID %d and its %d\n", i, (int)getpid(),
(int)getppid());
 write(1, message, strlen(message) + 1);
 read(0, read, strlen(message));
 wait(&status);
 fprintf(stderr, "Current process = %d, data =%s", (int)getpid(), read);
 exit(0);
 }
