#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "student.h"

int main(int argc,char *argv[]){

    student *s1;
    int data_size = sizeof(student)*NUMBER_STUDENTS;

	//hasnt O_CREAT
    int fd = shm_open(SHM_NAME, O_RDWR, S_IRUSR|S_IWUSR);

    if( fd == -1)
    {
        printf("SHM_OPEN failed \n");
        exit(-1);
    }

    if( ftruncate(fd, data_size) == -1)
    {
        printf("Ftruncate failed \n");
        exit(-1);
    }

	//fazer cast para o apontador 
    s1 = (student*) mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	printf("Student number: %d\n", s1->SNumber);
    printf("Student name: %s", s1->SName);
   
    if(munmap(s1, data_size) < 0){
        printf("munmap error!\n");
        exit(-1);
    }
    
    if (close(fd) < 0) {
		perror("Close error!\n");
		exit(-1);
	}
    
    if (shm_unlink(SHM_NAME) < 0) {
		perror("shm_unlink error!\n");
		exit(-1);
	}
    
    return 0;
}
