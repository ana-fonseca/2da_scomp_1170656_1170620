#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "student.h"

int main(){

    int data_size = sizeof(student) * NUMBER_STUDENTS;
    student *s1;
    
    int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

    if (fd == -1)
    {
        printf("SHM_OPEN failed!");
        exit(-1);
    }

    if (ftruncate(fd, data_size)==-1)
    {
        printf("Ftruncate failed");
        exit(-1);
    }

    s1 = (student*) mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

    if (s1 == MAP_FAILED){
        printf("Mmap error");
        exit(-1);
    }

    printf("\nStudent number: ");
    scanf("%d", &s1->SNumber);

    printf("\nThe student name: ");
    fgets(s1->SName, NAME_LENGTH, stdin);
	
    if(munmap(s1, data_size) < 0){ //error
        printf("Munmap error \n");
        exit(-1);
    }

    if(close(fd) < 0){
        printf("Close error \n");
        exit(-1);
    }

    return 0;
}
