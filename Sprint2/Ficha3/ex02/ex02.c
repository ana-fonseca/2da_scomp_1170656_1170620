#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>


//shared memo
#define SHM_NAME "/shmex02"

#define ARRAY_SIZE 1000000

typedef struct{
	int number;
	char flag; // wait
} Data;

int main(){

	pid_t pid;
	clock_t start, end;

	int open_shm = shm_open(SHM_NAME, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if(open_shm == -1){
		perror("Failed to create shm\n");
		exit(EXIT_FAILURE);
	}
	
	int size = sizeof(Data);
	int i;
	if(ftruncate(open_shm, size) == -1){
		perror("Failed ftruncate function\n");
		exit(EXIT_FAILURE);
	}
	
	Data* shm_data = (Data*) mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, open_shm, 0);
	if(shm_data == MAP_FAILED){
		perror("Failed mmap\n");
		exit(EXIT_FAILURE);
	}
	
	pid = fork();
	if(pid == -1){
		perror("Fork failed\n");
		exit(EXIT_FAILURE);
		
	}else if(pid > 0){
		
		//Initializes randomizer
		srand((unsigned) time(NULL));
		
		start = clock();
		
		shm_data->number = rand() %  10;
		shm_data->flag = 1;
				
		//fill
		for(i = 1; i < ARRAY_SIZE; i++){
			while(shm_data->flag == 1);
			shm_data->number = rand() %10;	
			shm_data->flag = 1;
		}
		
		
		wait(NULL);
	
	}else{ //child
				
		for(i = 0; i < ARRAY_SIZE; i ++){
			while(shm_data->flag == 0);
			//simulates reading
			shm_data->flag = 0;
			
		}
		
		exit(0);
	}
	
	end = clock();
	printf("Time: %f\n", (float) (end - start) / CLOCKS_PER_SEC);
	
	
	if(munmap(shm_data, size) == -1){
		perror("Failed munmap\n");
		exit(EXIT_FAILURE);
	}
	
	if(close(open_shm) == -1){
		perror("Didn't close correctly.");
		exit(EXIT_FAILURE);
	}
	
	shm_unlink(SHM_NAME);
	
	
//PIPES:
	
	int fd[2];
	
	if(pipe(fd) == -1){
		perror("Failed Pipe!\n");
		exit(EXIT_FAILURE);
	}
	
	pid_t pidPipe;
	
	if((pidPipe = fork()) == -1){
		perror("Failed to create child");
		exit(0);
	}
	
	if(pidPipe == 0){
		
		close(fd[1]);
			
		for(i = 0; i < ARRAY_SIZE; i++){
			int readNum;
			
			if(read(fd[0], &readNum, sizeof(int)) == -1){
				perror("Didn't read.");
				exit(EXIT_FAILURE);
			}
		}
		
		close(fd[0]);
		exit(0);
		
	}else{
		close(fd[0]);
		start = clock();
		
		for(i = 0; i < ARRAY_SIZE; i++){
			int numWrite = rand() % 10;
			
			if(write(fd[1], &numWrite, sizeof(int)) == -1){
				perror("Didn't Write");
				exit(EXIT_FAILURE);
			}
		}
		
		close (fd[1]);
		wait(NULL);		
		
	}
		
	
	end = clock();
	
	printf("Time: %f\n", (float) (end-start) / CLOCKS_PER_SEC);
	
	
	
return 0;
}
			
			
			
