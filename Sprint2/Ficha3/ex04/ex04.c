#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#define REPETION 1000000
#define SHM_NAME "/shmex04"


typedef struct{
	int number;
} Data;

int main(){

	int size = sizeof(Data);
	int i;
	
	int fd = shm_open(SHM_NAME, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if(fd == -1){
		perror("Failed shm_open\n");
		exit(EXIT_FAILURE);
	}
	
	if(ftruncate(fd, size) == -1){
		perror("Failed ftruncate\n");
		exit(EXIT_FAILURE);
	}
	
	Data* dados = (Data*) mmap(NULL,size, PROT_READ |PROT_WRITE, MAP_SHARED, fd, 0);
	if(dados == MAP_FAILED){
		perror("Failed mmap\n");
		exit(EXIT_FAILURE);
	}
	
	//Definir 100 inicialmente
	dados->number = 100;
	
	pid_t pid;
	
	if((pid = fork()) == -1){
		perror("Fork Failed\n");
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0){
		for(i = 0; i < size; i++){
			
			dados->number += 1;
			dados->number -= 1;
		}
		
		printf("Parent final value: %d\n", dados->number);
		
		exit(0);
	
	}else{
		
		for(i = 0; i < size; i++){
			
			dados->number += 1;
			dados->number -= 1;
		}
		
		printf("Child final value: %d\n", dados->number);
		
		exit(0);
			
	}
	
	if(munmap(dados, size) == -1){
		perror("Failed munmap\n");
		exit(EXIT_FAILURE);
	}
	
	if(close(fd) == -1){
		perror("Failed close\n");
		exit(EXIT_FAILURE);
	}
	
	shm_unlink(SHM_NAME);
	
	return 0;
}
