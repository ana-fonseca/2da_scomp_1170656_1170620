#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <sys/wait.h>

#define NUM_ELEMS 1000
#define SHARED_ARRAY_SIZE 10
#define NUM_CHILDS 10
#define SHM_NAME "/shmF3Ex7"
#define LIMIT 1000

int main (){

    int numbers[NUM_ELEMS], fd[2], maxLocal[SHARED_ARRAY_SIZE];
    int i, range, status, maxAll=0;
    pid_t listPids[NUM_CHILDS];    
    time_t t;
    
    /* intializes RNG (srand():stdlib.h; time(): time.h) */    
    srand((unsigned) time (&t));
    
    for(i = 0;i < NUM_ELEMS ;i++ ){
        numbers[i] = (rand () % (LIMIT+1)) ;//+1 porque nao é inclusive do lado direito
    }
    
    if(pipe(fd) == -1){
        printf("Pipe error!\n");
        exit(EXIT_FAILURE);
    }

    range = NUM_ELEMS/NUM_CHILDS;

    for(i = 0;i < NUM_CHILDS;i++){
        listPids[i] = fork(); //wait depois de todos os filhos serem criados

        if(listPids[i]==-1){
            printf("fork error\n");
            exit(-1);
        }

        if(listPids[i] == 0){
            int max = 0, c;
            int rangeInit = i * range; //range = NUM_ELEMS/NUM_CHILDS -> how many for each child
            int rangeFinal = rangeInit + range;
            //each son calculate which is the max in its range
            for(c = rangeInit;c < rangeFinal;c++){
                if(numbers[c]>max){
                    max = numbers[i];
                }
            }
			
            close(fd[0]);
			write(fd[0], &max ,sizeof(int));
			close(fd[1]);
            
            exit(1);   
        }
    }

    for(i = 0;i < NUM_CHILDS;i++){
        waitpid(listPids[i], &status, 0);
        if(!WIFEXITED(status)){
            printf("Error in son number %d. \n", i+1 );
        }else{
			close(fd[1]);
			read(fd[1], maxLocal[i] ,sizeof(int));
			close(fd[0]);
		}
    }

    //search for max
    for(i = 0;i < SHARED_ARRAY_SIZE;i++){
        if(maxAll < maxLocal[i]){
            maxAll = maxLocal[i];
        }
    }

    printf("Max: %d\n", maxAll);

    return 0;
}	
