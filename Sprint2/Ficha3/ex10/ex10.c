#include <sys/mman.h>
#include <sys/stat.h> 
#include <sys/types.h>
#include <fcntl.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>

#define SHM_NAME "shmex10"
#define N_NUMS 30
#define ARRAY_SIZE 10

typedef struct {
	int numbers[ARRAY_SIZE];
	int written;
	int nread;
} content;

int main(void){
	int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	if(fd == -1) {
		printf("Erro a criar Shared Memory\n");
		return 0;
	}
	
	int size = sizeof(content);
	
	if(ftruncate(fd, size) == -1) {
		printf("Erro a alocar espaço na Shared Memory.\n");
		return 0;
	}
	
	content *values = (content*) mmap(NULL,size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	if(values == NULL) {
		printf("Erro a mapear objeto.");
		return 0;
	}
	
	values->written = 0;
	values->nread = 0;
	
	if(fork() == 0) {
		srand((unsigned) time(NULL));
		while(values->written < N_NUMS) {
			while(values->written - values->nread >= 10);
			
			values->numbers[values->written %10]= rand() % 1000;
			values->written++;
		}
		wait(NULL);
	} else {		
		while(values->nread < N_NUMS) {
			while(values->nread >= values->written);
			
			printf("number[%d] = %d\n", values->nread + 1, values->numbers[values->nread %10]);
			values->nread++;
		}
		exit(0);
	}
	
	if(munmap(values, size) == -1) {
		printf("Unmapping error\n");
		return 0;
	}
	
	if(close(fd) == -1) {
		printf("Shared Memory closing error.\n");
		return 0;
	}
	shm_unlink(SHM_NAME);
	
	return 0;
}
