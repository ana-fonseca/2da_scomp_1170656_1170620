#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "shared.h"

#define LIMIT 20

int main(){

    int data_size = sizeof(int) * N_NUMBERS;
    int c;
    time_t t;
    int *s1;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */    
    srand((unsigned) time (&t));
    
    int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

    if (fd == -1)
    {
        printf("SHM_OPEN failed!");
        exit(-1);
    }

    if (ftruncate(fd, data_size)==-1)
    {
        printf("Ftruncate failed");
        exit(-1);
    }

    s1 = (int*) mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

    if (s1 == MAP_FAILED){
        printf("Mmap error");
        exit(-1);
    }

	for(c=0; c < N_NUMBERS; c++){
		
		s1[c] = (rand () % (LIMIT)) +1;//é preciso fazer o limit -1 e depois +1 para ser de 1 a 20
		
	}
	
    if(munmap(s1, data_size) < 0){ //error
        printf("Munmap error \n");
        exit(-1);
    }

    if(close(fd) < 0){
        printf("Close error \n");
        exit(-1);
    }

    return 0;
}
