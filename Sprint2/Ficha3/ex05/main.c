#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/wait.h>

#define NUM_ELEMS 1000
#define SHARED_ARRAY_SIZE 10
#define NUM_CHILDS 10
#define SHM_NAME "/shmF3Ex5"
#define LIMIT 1000

int main (){

    int numbers[NUM_ELEMS];
    int dataSize = sizeof(int)*SHARED_ARRAY_SIZE;
    int *ptr, i, range, status, maxAll=0;
    pid_t listPids[NUM_CHILDS];    
    time_t t;
    
    /* intializes RNG (srand():stdlib.h; time(): time.h) */    
    srand((unsigned) time (&t));
    
    for(i = 0;i < NUM_ELEMS ;i++ ){
        numbers[i] = (rand () % (LIMIT+1)) ;//+1 porque nao é inclusive do lado direito
    }

    int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

    if( fd == -1){
        printf("Error in fd \n");
        exit(-1);
    }

    if( ftruncate(fd, dataSize) == -1){
        printf("Ftruncate Error \n");
        exit(-1);
    }

    ptr = (int*) mmap(NULL, dataSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

    if( ptr == MAP_FAILED){
        printf("Mmap error \n");
        exit(-1);
    }

    range = NUM_ELEMS/NUM_CHILDS;

    for(i = 0;i < NUM_CHILDS;i++){
        listPids[i] = fork(); //wait depois de todos os filhos serem criados

        if(listPids[i]==-1){
            printf("fork error\n");
            exit(-1);
        }

        if(listPids[i] == 0){
            int max = 0, c;
            int rangeInit = i * range; //range = NUM_ELEMS/NUM_CHILDS -> how many for each child
            int rangeFinal = rangeInit + range;
            //each son calculate which is the max in its range
            for(c = rangeInit;c < rangeFinal;c++){
                if(numbers[c]>max){
                    max = numbers[i];
                }
            }
            
            //in the shared array write the max value in its range
            *(ptr+i)=max;
            exit(1);   
        }
    }

    for(i = 0;i < NUM_CHILDS;i++){
        waitpid(listPids[i], &status, 0);
        if(!WIFEXITED(status)){
            printf("Error in son number %d. \n", i+1 );
        }
    }

    //search for max
    for(i = 0;i < SHARED_ARRAY_SIZE;i++){
        if(maxAll < *(ptr+i)){
            maxAll = *(ptr+i);
        }
    }

    printf("Max: %d\n", maxAll);

    if( munmap(ptr, dataSize) < 0 ){
        printf("Munmap error \n");
        exit(-1);
    }

    if( shm_unlink(SHM_NAME)<0){
        printf("Shm_unlink error \n");
        exit(-1);
    }

    return 0;
}	
