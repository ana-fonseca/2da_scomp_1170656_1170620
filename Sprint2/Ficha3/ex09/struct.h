#define PATH_SIZE 100
#define WORD_SIZE 30
typedef struct
{
    char word[WORD_SIZE];
    int occurrences;
    char pathFile[PATH_SIZE];
    
} Search;