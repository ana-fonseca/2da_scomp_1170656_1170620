#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include "struct.h"

#define SHM_NAME "/shmex09"
#define PATH_NUMBER 10
#define LINE_LENGTH 80
#define CHILDS 10

/*Metodo para gerar os nomes dos ficheiros*/
void generatePathNames(char*vec){
    int i;
    for(i = 0;i < PATH_NUMBER;i++){
        sprintf((vec+i*PATH_SIZE), "files/file%d.txt", i);
    }
}

/*Metodo para gerar as palavras a procurar*/
void generateWordsToSearch(char*vec){
    int i;
    for(i = 0;i < CHILDS;i++){
        sprintf((vec+i*WORD_SIZE), "write%d", i);
    }
}

int main(){

    Sarch *sch;

    char pathVec[PATH_NUMBER][PATH_SIZE];
    char wordsToSearch[CHILDS][WORD_SIZE];

    generatePathNames(&pathVec[0][0]); //gerar o path de varios ficheiros
    generateWordsToSearch(&wordsToSearch[0][0]); //gerar as varias palavras a procurar

    int data_size = sizeof(search)*CHILDS;

    int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

    if(fd == -1){
        perror("SHM open fails! \n");
        exit(EXIT_FAILURE);
    }

    if(ftruncate(fd,data_size)==-1){
        perror("Ftruncate fails! \n");
        exit(EXIT_FAILURE);
    }

    sch = (search*) mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

    if(sch == MAP_FAILED){
        perror("MMAP fails! \n");
        exit(EXIT_FAILURE);
    }

    search *ptr = sch; //fazer o copia do apontador da memoria partilhar, para guardar a sua posicao inicial
    int i;

    //preencher a memoria partilhada com a informacao para cada filho
    for(i = 0;i < CHILDS;i++){
        ptr->occurrences = 0;
        strcpy(ptr->word, wordsToSearch[i]);
        strcpy(ptr->pathFile, pathVec[i]);
        ptr++;
    }

    pid_t pids[CHILDS]; //array to storage pid number of child processes

    ptr = sch; //colocar ptr apontar de novo para o inicio da memoria partilhada

    // vai criar os 10 processos filhos
    for(i = 0;i < CHILDS;i++){
        pids[i] = fork();

        if(pids[i]==-1){
            printf("Fork filho %d falhou! \n", i);
            exit(EXIT_FAILURE);
        }

        if(pids[i]==0){ //filho
            ptr+=i;
            FILE *file = fopen(ptr->pathFile, "r");
            //https://codeforwin.org/2018/02/c-program-count-occurrences-of-a-word-in-file.html
            if(file){
                char line[LINE_LENGTH];
                char *pos;
                int indice;
                ptr->occurrences = 0;
                while(fgets(line, LINE_LENGTH, file) != NULL ){  
                    indice = 0;
                    while( (pos = strstr(line + indice, ptr->word)) != NULL){
                        indice = (pos - line) + 1;
                        ptr->occurrences++;
                    }
                }
            } else {
                printf("Ficheiro não econtrado !\n");
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
    }

    //espera que todos os filhos terminem
    int status;
    for(i = 0;i < CHILDS;i++){
        waitpid(pids[i], &status, 0);
    }

    //imprimir as pesquisas de cada filho
    for(i = 0;i < CHILDS;i++){
        printf("Filho %d  Palavra: %s   Encontrada= %dx\n", i, ptr->word, ptr->occurrences);
        ptr++;
    }

    if(munmap(sch, data_size)==-1){
        perror("munmap fails! \n");
        exit(EXIT_FAILURE);
    }

    if(shm_unlink(SHM_NAME)==-1){
        perror("shm_unlink fails! \n");
        exit(EXIT_FAILURE);
    }

    return 0;
}

