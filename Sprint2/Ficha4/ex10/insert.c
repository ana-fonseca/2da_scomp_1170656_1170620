#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <semaphore.h>

#define MAX_RECORDS 100
#define SIZE 30


typedef struct{
	int number;
	char name[SIZE];
	char address[SIZE];
}personalRecords;

typedef struct{
	personalRecords record[MAX_RECORDS];
	int index;
}Data;


char *shm_name = "/shmex10";
char *sem_name = "/semex10";
int sem_value = 1;



int main(){
	
	//shared memory
	int fd = shm_open(shm_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if(fd == -1){
		perror("Creating shared memory error\n");
		exit(0);
	}
	
	int size = sizeof(Data);
	
	if(ftruncate(fd, size) == -1){
		perror("Erro no ftruncate\n");
		exit(0);
	}
	
	Data *sd = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	if(sd == NULL){
		printf("Erro no mmap\n");
		exit(0);
	}
	
	//semaphore
	sem_t *sem;
	
	sem = sem_open(sem_name, O_CREAT, 0644, sem_value);
	if(sem == SEM_FAILED){
		printf("Erro no sem_open\n");
		return 0;
	}
	
	//event: read info
	sem_wait(sem);
	
	personalRecords *rec = &(sd->record[sd->index]);
	
	printf("Number(ID):\n");
	scanf("%d", &(rec->number));
	fflush(0);
	
	printf("Name:\n");
	char aux[SIZE];
	scanf("%s", aux);
	strcpy(rec->name, aux);
	fflush(0);
	
	printf("Address:\n");
	
	scanf("%s", aux);
	strcpy(rec->address, aux);
	fflush(0);
	
	//must know index
	sd->index++;
	
	sem_post(sem);
	
	
	//closing
	sem_unlink(sem_name);
	
	munmap(sd, size);
	close(fd);
	
	return 0;
}
