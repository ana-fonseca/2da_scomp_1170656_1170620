#ifndef UTILS_SHM_SEMS_H
#define UTILS_SHM_SEMS_H
void* shm_create(char* name, int data_size);
void* shm_create_excl(char* name, int data_size);
#endif
