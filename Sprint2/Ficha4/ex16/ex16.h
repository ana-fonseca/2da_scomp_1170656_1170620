#define SHM_NAME "/shm_ex16"
#define N_SEMS 3
#define CROSS_TIME 30 //30 sec para passar a ponte
#define SEM_MUTEX 0
#define SEM_EAST 1
#define SEM_WEST 2

char sems_names[][25] = {"/sem_ex16_mutex", "/sem_ex16_east", "/sem_ex16_west"};
int sems_init_values[] = {1, 1, 1}; 

typedef struct {
    int nWestCrossing;
    int nEastCrossing;
} shared_data;
