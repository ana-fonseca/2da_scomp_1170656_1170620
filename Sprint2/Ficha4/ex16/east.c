#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <string.h>
#include <semaphore.h>
#include "utils_shm_sems.h"
#include "ex16.h"

int main(void) {

    shared_data *shm;
    int data_size = sizeof(shared_data);
    sem_t *sems[N_SEMS];
    int i;

    shm = (shared_data*) shm_create(SHM_NAME, data_size);

    for (i = 0; i < N_SEMS; i++) {
        if ((sems[i] = sem_open(sems_names[i], O_CREAT, 0644, sems_init_values[i])) == SEM_FAILED) { 
		    perror("Error in sem_open()");
		    exit(EXIT_FAILURE);
	    }
    }

    sem_wait(sems[SEM_EAST]); //esperar ate ser possivel passar
    sem_post(sems[SEM_EAST]); //mais carros podem passar na mesma direcao
    
    sem_wait(sems[SEM_MUTEX]);
    shm->nEastCrossing++;
    sem_post(sems[SEM_MUTEX]);

    if (shm->nEastCrossing == 1) { //se esta 1 a passar, a direçao contraria tem q esperar (== 1 para garantir que nao sao feitos multiplos waits e causa um deadlock)
        puts("Cars coming from east... waiting... ");
        sem_wait(sems[SEM_WEST]);
    }
    
    puts("Crossing from east...");
    sleep(CROSS_TIME);
    puts("Crossed yay");

    sem_wait(sems[SEM_MUTEX]);
    shm->nEastCrossing--; //ja passou
    sem_post(sems[SEM_MUTEX]);

    if (shm->nEastCrossing == 0) { // se nao existem mais a passar east, deixar passar west
        sem_post(sems[SEM_WEST]);
    }
    
    //*Fechar memoria partilhada
    if(munmap(shm, data_size ) == -1){
        perror("shm unmap failed ;(");
        exit(EXIT_FAILURE);
    }
	//*Fechar semaforos
	for	(i = 0; i < N_SEMS; i++) {
		if (sem_close(sems[i]) < 0) {
			perror("sem close failed :(");
			exit(EXIT_FAILURE);
		}
	}
    
    return EXIT_SUCCESS;
}
