#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <semaphore.h>

void* shm_create(char* name, int data_size) {
	void* ret;
	
	int fd = shm_open(name, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);

    if (fd == -1) {
        printf("SHM_OPEN failed :(\n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed :(\n");
        exit(EXIT_FAILURE);
    }

    ret = (void*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (ret == MAP_FAILED) {
        printf("mmap failed :/\n");
        exit(EXIT_FAILURE);
    }
    
    return ret;
}


void* shm_create_excl(char* name, int data_size) {
	void* ret;
	
	int fd = shm_open(name, O_CREAT|O_RDWR|O_EXCL, S_IRUSR|S_IWUSR);

    if (fd == -1) {
        printf("SHM_OPEN failed :(\n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed :(\n");
        exit(EXIT_FAILURE);
    }

    ret = (void*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (ret == MAP_FAILED) {
        printf("mmap failed :/\n");
        exit(EXIT_FAILURE);
    }
    
    return ret;
}






