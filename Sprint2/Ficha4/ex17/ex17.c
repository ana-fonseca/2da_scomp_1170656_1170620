#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/mman.h>

#define SEM_EMPTY_NAME "/sem_empty_ex17"
#define SEM_FULL 0
#define SEM_VIP 1
#define SEM_SPECIAL 2
#define SEM_NORMAL 3
#define SEM_THEATER 4

#define SHAREDMEMORY "/shmex17"
#define NUM_SEMAPHORES 5

#define SIZE 300

#define VIP 1
#define SPECIAL 2
#define NORMAL 3

typedef struct
{
    int vip_clients;
    int special_clients;
    int normal_clients;
} Theater;

char *sem_names[NUM_SEMAPHORES] = {"/sem_full_ex17",
									"/sem_vip_ex17", "/sem_special_ex17",
									"/sem_normal_ex17", "/sem_theater_ex17"};

int main(){

    pid_t pid;
    sem_t *sem_empty;
    Theater *t;

    int fd, data_size = sizeof(Theater);

    fd = shm_open(SHAREDMEMORY, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR); //criação de ficheiro de memoria partilhada para escrita
    if (fd == -1){
        perror("Erro partilha de memoria Writer");
    }
    int memoryAmount = ftruncate(fd, data_size); // definição de memoria a ser partilhada
    if (memoryAmount == -1){
        perror("Erro a definir tamanho de memoria");
    }

    t = (Theater *)mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0); //endereçamento para a estrutura "t" a memoria que foi partilhada

    if (t == MAP_FAILED){
        perror("Erro no endereçamento de memoria partilhada");
    }

	sem_t *sem[NUM_SEMAPHORES];
	int i;
	for(i=0; i< NUM_SEMAPHORES; i++){
		sem [i] = sem_open(sem_names[i], O_CREAT | O_EXCL, 0644, 0);
		if (sem[i] == SEM_FAILED){
			perror("Can't sem_open() sem_empty\n");
		}
	}
	
    sem_empty = sem_open(SEM_EMPTY_NAME, O_CREAT | O_EXCL, 0644, SIZE);
    if (sem_empty == SEM_FAILED){
        perror("Can't sem_open() sem_empty\n");
    }

    
    pid = fork();
    
    if (pid == -1){
        perror("fork error");
    }
    
    if (pid > 0){ //PARENT
        int total_people;
        while (1)
        {
            sem_getvalue(sem_empty, &total_people); // check if its full
            if (total_people == SIZE){
				
                printf("Theater is FULL, Someone exits...\n");
                sleep(2);
                sem_wait(sem[SEM_FULL]); //--
                sem_post(sem_empty);
                
                if (t->vip_clients > 0){ //garante que há vips à espera
                    printf("VIP client can enter\n");
                    sem_post(sem[SEM_VIP]); 
                }
                if (t->special_clients > 0){
                    printf("SPECIAL client can enter\n");
                    sem_post(sem[SEM_SPECIAL]); 
                }
                if(t->normal_clients>0){
                    printf("NORMAL client can enter\n");
                    sem_post(sem[SEM_NORMAL]); 
                }
               
            }
            else
            {
                sem_wait(sem[SEM_FULL]);
                sem_post(sem_empty);
            }
        }
    }
    else
    { //CHILD
        
        int total_people;
        while (1){
			
            sem_getvalue(sem[SEM_FULL], &total_people); 
            
            if (total_people == SIZE){
				
                int client_status = (rand() % 3) + 1; 
                if (client_status == VIP){
                    printf("Theater full , VIP client Waiting\n");
                    t->vip_clients++; //acrescenta um vip à "lista de espera"
                    sleep(1);
                    sem_wait(sem[SEM_VIP]);
                    t->vip_clients--;
                    printf("Theater is now free , VIP client has entered!\n");
                    sleep(1);
                }
                if (client_status == SPECIAL)
                {
                    printf("Theater full , SPECIAL client Waiting\n");
                    t->special_clients++; //o mesmo que o vip
                    sleep(1);
                    sem_wait(sem[SEM_SPECIAL]); 
                    t->special_clients--;
                    printf("Theater is now free , SPECIAL client has entered!\n");
                    sleep(1);
                }
                if (client_status == NORMAL)
                {
                    printf("Theater full , NORMAL client Waiting\n");
                    t->normal_clients++;
                    sleep(1);
                    sem_wait(sem[SEM_NORMAL]);
                    t->normal_clients--;
                    printf("Theater is now free , NORMAL client has entered!\n");
                    sleep(1);
                }
                sem_wait(sem_empty);
                sem_post(sem[SEM_FULL]);
            }
            else
            {
                sem_wait(sem_empty);
                sem_post(sem[SEM_FULL]);
            }
        }
        exit(0);
    }


	
	int p;
	for(p=0; p<NUM_SEMAPHORES; p++){
		sem_unlink(sem_names[p]);
	}
	
   sem_unlink(SEM_EMPTY_NAME);


    if (munmap(t, data_size) == -1){
        perror("Erro unmap\n");
    }
    if (close(fd) == -1){ 
        perror("Erro close\n");
    }
    if (shm_unlink(SHAREDMEMORY) == -1){
        perror("Erro unlink\n");
    }


    return 0;
}
