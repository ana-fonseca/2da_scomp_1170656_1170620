#define SHM_NAME "/shm_ex12"
#define N_TICKETS 50
#define N_SEMS 2
#define SEM_MUTEX 0
#define SEM_ATENDER 1
#define SLEEP_TIME_MIN 1
#define SLEEP_TIME_MAX 10

char sems_names[][25] = {"/sem_ex12_mutex", "/sem_ex12_atender"};
int sems_init[] = {1, 1};

typedef struct {
    int nextTicket;
    int nextServed;
} shared_data;
