#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <string.h>
#include <semaphore.h>
#include <time.h>
#include "ex12.h"

int main(void) {

    shared_data* shared_ptr;
    int data_size = sizeof(shared_data);

    sem_t* sems[N_SEMS];
    int i, timeWait;
	
	srand (time (NULL)*getpid());
	
	int fd = shm_open(SHM_NAME, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);

    if (fd == -1) {
        printf("SHM_OPEN failed :(\n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed :(\n");
        exit(EXIT_FAILURE);
    }

    shared_ptr = (shared_data*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (shared_ptr == MAP_FAILED) {
        printf("mmap failed :/\n");
        exit(EXIT_FAILURE);
    }

    //*Abrir semaforos
    for (i = 0; i < N_SEMS; i++) {
        if ((sems[i] = sem_open(sems_names[i], O_CREAT, 0644, sems_init[i])) == SEM_FAILED) { 
		    perror("Error in sem_open()");
		    exit(EXIT_FAILURE);
	    }
    }
    
    //Getting ticket
    sem_wait(sems[SEM_MUTEX]);
    if (shared_ptr->nextTicket > N_TICKETS) {
		printf("Couldn't get a ticket. Alredy exists %d tickets!",shared_ptr->nextTicket);
		sem_post(sems[SEM_MUTEX]);
	} else {
		int myTicket = shared_ptr->nextTicket;
		printf("My ticket is = %d\n", myTicket);
		shared_ptr->nextTicket++;
		sem_post(sems[SEM_MUTEX]);
		
		//Being Served
		int served = 0;
		
		while (served == 0) {
			
			sem_wait(sems[SEM_ATENDER]);
			
			if (shared_ptr->nextServed == myTicket) {
				timeWait=rand () % (SLEEP_TIME_MAX + 1 - SLEEP_TIME_MIN) + SLEEP_TIME_MIN;
				printf("Being served... (duration = %d secs)\n",timeWait);
				sleep(timeWait);
				served = 1;
				
				sem_wait(sems[SEM_MUTEX]);
				shared_ptr->nextServed++; //next person allowed
				sem_post(sems[SEM_MUTEX]);
				
				printf("My ticket is = %d.\n", myTicket);
			}
			
			sem_post(sems[SEM_ATENDER]);
		}
		exit(EXIT_SUCCESS);
	}
	
	
    //*Fechar memoria partilhada
    if(munmap(shared_ptr, data_size ) == -1){
        perror("shm unmap failed ;(");
        exit(EXIT_FAILURE);
    }
	//*Fechar semaforos
	for	(i = 0; i < N_SEMS; i++) {
		if (sem_close(sems[i]) < 0) {
			perror("sem close failed :(");
			exit(EXIT_FAILURE);
		}
	}

    exit(EXIT_SUCCESS);
}
