#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <sys/wait.h>
#include <semaphore.h>
#include <string.h>
#include "ex03.h"
#include <time.h>
#include <wait.h>


#define limite 10

int main(void) {
	
	shared_data *shared_ptr;
	char str[STRING_SIZE];
	sem_t *sem;
	int tempoEspera;
	time_t t;						/* needed to init. the random number generator (RNG)*/
	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time (&t));
	
	int data_size = sizeof(shared_data);
	
	int fd = shm_open(SHM_NAME, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR); //usa a memoria partilhada se ja existir

    if (fd == -1) {
        printf("SHM_OPEN failed! \n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed! \n");
        exit(EXIT_FAILURE);
    }
	
    shared_ptr = (shared_data*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (shared_ptr == MAP_FAILED) {
        printf("mmap failed! \n");
        exit(EXIT_FAILURE);
    }
   
	if ((sem = sem_open("/sema", O_CREAT, S_IRUSR|S_IWUSR, 1)) == SEM_FAILED) { //criacao exclusiva
		perror("Error in sem_open()");
		exit(EXIT_FAILURE);
	}
	
	sem_wait(sem); // semaforo--
	//critical zone
	int index = shared_ptr -> nStrings; //get next available index
	shared_ptr -> nStrings++;
	
	sprintf(str, "I'm process Father- with PID %d\n", (int) getpid());
	strncpy(shared_ptr -> strings[index], str, STRING_SIZE);
	
	puts(str);
	printf("Wrote on index: %d\n", index);
	//end of critical zone
	sem_post(sem);// semaforo++
	
	tempoEspera = (rand() % (limite-1))+1;	 
	printf("Vai ficar à espera %d segundos...\n",tempoEspera);
	
	sleep(tempoEspera); //sleep 
	
	return EXIT_SUCCESS;
}
