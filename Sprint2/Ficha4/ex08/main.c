#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <sys/wait.h>
#include <semaphore.h>
#include <string.h>
#include "ex08.h"

#define N_SEMS 2
#define SEM_PAI 0
#define SEM_FILHO 1
#define N_IT 50

int main(void) {

    char sems_names[][25] = {"/sem_ex08_pai", "/sem_ex08_filho"};
    int sems_init_values[] = {0, 0};
    sem_t* sems[N_SEMS];
    int i;

    puts("");
    
    for (i = 0; i < N_SEMS; i++) {
        if ((sems[i] = sem_open(sems_names[i], O_CREAT, 0644, sems_init_values[i])) == SEM_FAILED) { 
		    perror("Error in sem_open()");
		    exit(EXIT_FAILURE);
	    }
    }

    pid_t pid = fork();
    if (pid < 0) {
        perror("fork() failed :(");
        exit(EXIT_FAILURE);
    }
    int repeated = 0;

    if (pid == 0) { //filho

        for (i = 0; i < N_IT; i++) {
            if (repeated < 2) {
                usleep(200);
                printf("C");
                fflush(stdout);

                sem_post(sems[SEM_PAI]);
                repeated++;
            } else {
                sem_wait(sems[SEM_FILHO]); //bloqueia ate o pai escrever
                repeated = 0; //volta a zero porque o pai escreveu
            } 
        }

    } else { //pai

        for (i = 0; i < N_IT; i++) {
            if (repeated < 2) {
                usleep(200);
                printf("S");
                fflush(stdout);

                sem_post(sems[SEM_FILHO]);
                repeated++;
            } else {
                sem_wait(sems[SEM_PAI]); //bloqueia ate o filho escrever
                repeated = 0; //volta a zero porque o filho escreveu
            } 
        }
        wait(NULL);
        puts("\n");
    }

    //close
    for(i = 0;i < N_SEMS; i++){
        if(sem_close(sems[i])==-1){
            perror("sem_close failed ;(");
            exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}