#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <time.h>

#define N_SEMAPH 4
#define MAX_PASSENGERS 200
#define N_PROCESS 3
#define SHM_NAME "/shm_ex11"

int main(){

    char sems_names[][25] = {"/sem_ex11_0", "/sem_ex11_1", "/sem_ex11_2", "/sem_ex11_3"};
    sem_t* sem[N_SEMAPH];
    int sems_init_values[] = {1, 1, 1, 1};
    
    srand (time (NULL)*getpid());

    //SEMAFOROS
    int i = 0;
    for(i = 0;i < N_SEMAPH;i++){
        if((sem[i] = sem_open(sems_names[i], O_CREAT, 0644, sems_init_values[i]))== SEM_FAILED){
            perror("sem_open fails!\n");
            exit(EXIT_FAILURE);
        }
    }

    //MEMORIA PARTILHADA
    int data_size = sizeof(int*);
	int *ptr_shared; //poiter to shared memory
	
	int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

    if (fd == -1) {
        printf("SHM_OPEN failed :(\n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed :(\n");
        exit(EXIT_FAILURE);
    }

    ptr_shared = (int*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (ptr_shared == MAP_FAILED) {
        printf("mmap failed :/\n");
        exit(EXIT_FAILURE);
    }
    
    //*CORPO PRINCIPAL DO EXERCICIO
    for (i = 0; i < N_PROCESS; i++) {
        pid_t pid= fork();
        
        if(pid == -1){
            perror("Fork fails\n");
            exit(EXIT_FAILURE);
        }
        
        if(pid == 0){
            int flag=1;
            do {
                sem_wait(sem[3]);//semaforo: bloqueia memoria partilhada
                if((*ptr_shared)<MAX_PASSENGERS){ //verifica se o comboio (shm) esta cheio
                    int num = rand () % (2);//ou 0 ou 1 -> gera numero para saber se entra um passageiro ou sai
                    sem_wait(sem[i]);//semaforo: bloqueia porta i
                    if (num==0 && (*ptr_shared)>0) {
                        (*ptr_shared)--;// sai um passageiro
                        printf("Porta %d saiu passageiro : TOTAL %d\n",i, (*ptr_shared));
                    } else if( num==1){
                        (*ptr_shared)++;//entra um passageiro
                        printf("Porta %d entrou passageiro : TOTAL %d\n",i, (*ptr_shared));
                    }
                    sem_post(sem[i]);//semaforo: liberta a porta i
                } else {
                    flag=0;//se o comboio já está cheio o porta não abre mais (filho termina)
                }
                sem_post(sem[3]);//semaforo: liberta memoria partilhada
            } while (flag);
            exit(EXIT_SUCCESS);
        }
    }

    //wait for all child processes
    for (i = 0; i < N_PROCESS; i++) {
        wait(NULL); 
    }

    //FECHAR MEMORIA PARTILHADA
    if (munmap(ptr_shared, data_size) < 0){
        perror("munmap error!\n");
        exit(EXIT_FAILURE);
    }

    if (shm_unlink(SHM_NAME) < 0) {
        perror("shm_unlink error!\n");
        exit(EXIT_FAILURE);
    }

    //FECHAR SEMAFOROS
    for(i = 0;i < N_SEMAPH;i++){
        if(sem_close(sem[i])==-1){
            printf("sem_close failed: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }
    
    for(i = 0;i < N_SEMAPH;i++){
        if(sem_unlink(sems_names[i])==-1){
            printf("sem_unlink failed: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
