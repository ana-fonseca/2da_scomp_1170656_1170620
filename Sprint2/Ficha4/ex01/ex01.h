#define SHM_NAME "/shm_ex01"
#define NUM_PROC 8
#define N_STRINGS_EACH 200
#define STRING_SIZE 80

typedef struct {
	char strings[N_STRINGS_EACH*NUM_PROC][STRING_SIZE];
	int nStrings;
} shared_data;
