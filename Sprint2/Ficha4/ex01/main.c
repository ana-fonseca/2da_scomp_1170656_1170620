#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <sys/wait.h>
#include <semaphore.h>
#include <string.h>
#include "ex01.h"
#include <time.h>
#include <wait.h>



int main(void) {
	
	shared_data *shared_ptr;
	char str[STRING_SIZE];
	sem_t *sem;
	int i;
	pid_t p;
	
	int data_size = sizeof(shared_data);
	
	int fd = shm_open(SHM_NAME, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR); //usa a memoria partilhada se ja existir

    if (fd == -1) {
        printf("SHM_OPEN failed! \n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed! \n");
        exit(EXIT_FAILURE);
    }
	
    shared_ptr = (shared_data*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (shared_ptr == MAP_FAILED) {
        printf("mmap failed! \n");
        exit(EXIT_FAILURE);
    }
   
	if ((sem = sem_open("/sema", O_CREAT, S_IRUSR|S_IWUSR, 1)) == SEM_FAILED) { //criacao exclusiva
		perror("Error in sem_open()");
		exit(EXIT_FAILURE);
	}
	
	for(i = 0; i < NUM_PROC;i++){
		
		if((p=fork())==-1){
			
			
		}else if(p>0){
			
			
			
		}else{
			sem_wait(sem); // semaforo--
			//critical zone
					
			int minLine=i*N_STRINGS_EACH;
			int maxLine=(i+1)*N_STRINGS_EACH;
			
			FILE *file = fopen("./test.txt", "r");
			if(file){
				int count = 0;	
				char line[STRING_SIZE]; 
				while (fgets(line, sizeof line, file) != NULL && count < maxLine) /* read a line */
				{
					if (count >= minLine ){
				
						int index = shared_ptr -> nStrings; //get next available index
						shared_ptr -> nStrings++;
						
						strncpy(shared_ptr -> strings[index], str, STRING_SIZE);
						
						puts(str);
						printf("Wrote on index: %d\n", index);
				
					}
					count++;
					
				}
				fclose(file);

            } else {
                printf("Ficheiro não econtrado !\n");
                exit(EXIT_FAILURE);
            }
			
			//end of critical zone
			sem_post(sem);// semaforo++
				
			
		}
		
		
	}
	
	if(munmap(shared_ptr, data_size ) == -1){
        perror("shm unmap failed ;(");
        exit(EXIT_FAILURE);
    }

    if (sem_close(sem) < 0) {
		perror("sem close failed :(");
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
