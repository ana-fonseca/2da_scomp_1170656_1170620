#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <time.h>
#include <fcntl.h>
#include "ex15.h"

#define SHM_NAME "/shm_ex15"
#define SEM_NAME "/sem_ex15"
#define N_VISITANTES 5
#define TURNOS 10

int main(){

    //SEMAFOROS
    sem_t* sem;
    if((sem = sem_open(SEM_NAME, O_CREAT | O_EXCL, 0644, 1))== SEM_FAILED){
        perror("sem_open fails!\n");
        exit(EXIT_FAILURE);
    }

    //MEMORIA PARTILHADA
    shared_data* shared;
	int data_size = sizeof(shared_data);
	
	int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);

    if (fd == -1) {
        printf("SHM_OPEN failed :(\n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed :(\n");
        exit(EXIT_FAILURE);
    }

    shared = (shared_data*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (shared == MAP_FAILED) {
        printf("mmap failed :/\n");
        exit(EXIT_FAILURE);
    }
	srand (time (NULL)*getpid());

    shared->aberto=1;
    shared->visitantes=0;

    pid_t pid = fork();

    if(pid == -1){
        printf("Filho erro fork\n");
        exit(EXIT_FAILURE);
    }

    int cont=0;
    if(pid==0){
        cont=0;
        while(cont<TURNOS){
            sleep(rand () % (4 ) + 1);
            printf("Show %d\n", cont+1);
            sem_wait(sem); //bloqueia memoria partilhada
            printf("Visitantes: %d\n", shared->visitantes);
            while (shared->visitantes>0) {//retirar os visitantes da room
                shared->visitantes--;
                printf("Saiu 1 visitante\tRestantes visitantes:%d\n", shared->visitantes);
            };
            printf("\n");
            sem_post(sem); //liberta a memoria partilhada
            cont++;
        }
        sem_wait(sem);
        shared->aberto=0; // show room fechou, informacao usada para nao aceitar mais visitantes 
        sem_post(sem);
        exit(EXIT_SUCCESS);
    }
    int flag=1;
    do {
        sem_wait(sem);
        if(shared->visitantes<N_VISITANTES && shared->aberto==1){ //verifica se ainda podem entrar visitantes e se o show room esta aberto
            shared->visitantes++;
            printf("Entrou 1 visitante\tTotal visitantes: %d\n",shared->visitantes);
        }
        if(!shared->aberto){ //se o showroom já fechou
            flag=0;
            printf("O show ja fechou!\n");
        }
        sem_post(sem);
    } while (flag);

    //FECHAR SEMAFORO
    if(sem_close(sem)==-1){
        printf("sem_close failed: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    if(sem_unlink(SEM_NAME)==-1){
        printf("sem_unlink failed: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    //FECHAR MEMORIA PARTILHADA
    if (munmap(shared, data_size) < 0){
        perror("munmap error!\n");
        exit(EXIT_FAILURE);
    }

    if (shm_unlink(SHM_NAME) < 0) {
        perror("shm_unlink error!\n");
        exit(EXIT_FAILURE);
    }

    return 0;
}
