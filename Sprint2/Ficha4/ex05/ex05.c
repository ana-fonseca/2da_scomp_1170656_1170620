#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <semaphore.h>

int main(){
	char *sem_name = "/semex05";
	int sem_value = 0;
	sem_t *sem;
	pid_t pid;
	
	sem = sem_open(sem_name, O_CREAT | O_EXCL, 0644, sem_value);
	if(sem == SEM_FAILED){
		printf("Error creating semaphore!\n");
		return 0;
	}
	
	pid = fork();
	if(pid == -1){
		printf("Error creating child proccess\n");
	}
	
	if(pid == 0) {
		printf("I'm the child\n");
			
		sem_post(sem);
		
		exit(0);
	}
	
	sem_wait(sem);
	
	printf("I'm the father\n");
	
	sem_unlink(sem_name);
	
	wait(NULL);
	return 0;
}
