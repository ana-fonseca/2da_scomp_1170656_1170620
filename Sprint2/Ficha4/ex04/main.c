#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <sys/wait.h>
#include <semaphore.h>
#include <string.h>
#include <time.h>
#include "ex03.h"

#define TIME_WAIT 12
#define limite 10

int main(void) {
	
	shared_data *shared_ptr;
	struct timespec ts;
	char str[STRING_SIZE];
	sem_t *sem;
	int data_size = sizeof(shared_data);
	int i, pos, s;
	int tempoEspera;
	time_t t;						/* needed to init. the random number generator (RNG)*/
	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time (&t));
	
	int fd = shm_open(SHM_NAME, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR); //usa a memoria partilhada se ja existir

    if (fd == -1) {
        printf("SHM_OPEN failed :(\n");
        exit(EXIT_FAILURE);
    }

    if (ftruncate(fd, data_size) == -1) {
        printf("ftruncate failed :(\n");
        exit(EXIT_FAILURE);
    }

    shared_ptr = (shared_data*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (shared_ptr == MAP_FAILED) {
        printf("mmap failed :/\n");
        exit(EXIT_FAILURE);
    }
    
	if ((sem = sem_open("/sema", O_CREAT, 0644, 1)) == SEM_FAILED) { //criacao exclusiva
		perror("Error in sem_open()");
		exit(EXIT_FAILURE);
	}

	if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
	{
		/* handle error */
		return -1;
	}      	
	
	printf("Will wait %d secs!\n",TIME_WAIT);
	
	ts.tv_sec += TIME_WAIT;
	
	int contError=0;
	while ((s = sem_timedwait(sem, &ts)) == -1){
		contError++;
		printf("Error in waiting...(error number = %d)\n",contError);
        continue;       /* Restart if interrupted by handler */
	}

    if (s == -1) {
		printf("sem_timedwait() timed out\n");
    } else{
        printf("sem_timedwait() succeeded\n");
    }
	
	printf("Waited %d secs!\n",TIME_WAIT);
	
	//critical zone
	int index = shared_ptr -> nStrings; //get next available index
	printf("Which line do you want to delete (starts at 0)? \n");
	scanf("%d",&pos);
	
	if (pos < index) { 
		//todas as strings descem 1 index
		for (i = pos; i < N_STRINGS - 1; i++) {
			strncpy (shared_ptr -> strings[i], shared_ptr -> strings[i + 1], STRING_SIZE); 
		}
		strncpy(shared_ptr -> strings[N_STRINGS - 1], "\0", STRING_SIZE); //ultima pos fica vazia
		shared_ptr -> nStrings--;
		printf("Deleted with success! \n");
	} else{
		printf("Didn't deleted any line! \n");
	}

	index = shared_ptr -> nStrings; //get next available index
	
	shared_ptr -> nStrings++;
	
	sprintf(str, "I'm process Father - with PID %d\n", (int) getpid());
	strncpy(shared_ptr -> strings[index], str, STRING_SIZE);
	
	puts(str);
	printf("Wrote on index: %d\n", index);
	//end of critical zone
	sem_post(sem);
	
	tempoEspera = (rand() % (limite-1))+1;	 
	printf("Vai ficar à espera %d segundos...\n",tempoEspera);
	
	sleep(tempoEspera); //sleep 
	
	return EXIT_SUCCESS;
}
