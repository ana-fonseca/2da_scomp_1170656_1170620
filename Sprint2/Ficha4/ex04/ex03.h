#define SHM_NAME "/shm_ex04"
#define N_STRINGS 50
#define STRING_SIZE 80

typedef struct {
	char strings[N_STRINGS][STRING_SIZE];
	int nStrings;
} shared_data;
