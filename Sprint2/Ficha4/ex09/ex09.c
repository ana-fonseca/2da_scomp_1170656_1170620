#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>

#define NUM_CHILD 2
#define SHM_NAME "shmex09"
char *sem_name = {"/sem_ex09"};
int sem_value = 0;


typedef struct {
	int proc_at_barrier;
} content;

int main(){
	int i;
	sem_t *sem_barrier, *sem;
	
	
		int fd = shm_open(SHM_NAME, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
		if(fd == -1) {
			printf("Erro a criar Shared Memory\n");
			return 0;
		}
	
		int size = sizeof(content);
	
		if(ftruncate(fd, size) == -1) {
			printf("Erro a alocar espaço na Shared Memory.\n");
			return 0;
		}
	
		content *value = (content*) mmap(NULL,size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
		if(value == NULL) {
			printf("Erro a mapear objeto.");
			return 0;
		}
	
		sem_barrier = sem_open(sem_name, O_CREAT | O_EXCL, 0644, sem_value);
		if(sem_barrier == SEM_FAILED){
			printf("Erro a criar semaforo!\n");
			return 0;
		}
		
		sem= sem_open("sem01", O_CREAT | O_EXCL, 0644, sem_value);
		if(sem_barrier == SEM_FAILED){
			printf("Erro a criar semaforo!\n");
			return 0;
		}
		
		
		value->proc_at_barrier = 0;
	
	for(i = 0; i < 2; i++) {
		if(fork() == 0) {
			if(i == 0) {
				printf("Child %d - buy_chips();\n", i);
				value->proc_at_barrier++;
				printf("entrou\n");
				sem_post(sem);
								
				if(value->proc_at_barrier == 2){
					sem_post(sem_barrier);
					printf("Child %d - eat_and_drink();\n", i);
				}
				
			} else {
				printf("Child %d - buy_beer();\n", i);
				value->proc_at_barrier++;
				printf("entrou\n");
				sem_wait(sem);
				
				sem_wait(sem_barrier);
				
				printf("Child %d - eat_and_drink();\n", i);
				
			}
			
			exit(0);
		}
	}
	
	for(i = 0; i < NUM_CHILD; i++)
		wait(NULL);
	
		sem_unlink(sem_name);
		sem_unlink("sem01");
		
	if(munmap(value, size) == -1) {
		printf("Unmapping error\n");
		return 0;
	}
	
	if(close(fd) == -1) {
		printf("Shared Memory closing error.\n");
		return 0;
	}
	shm_unlink(SHM_NAME);
	
	
	return 0;
}
