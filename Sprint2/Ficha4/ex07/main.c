#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define CHILDS 3
#define N_SEMAPH 3

int main(){

    char msgChild0[][25] = {"Sistemas ", "a "};
    char msgChild1[][25] = {"de ", "melhor "};
    char msgChild2[][25] = {"Computadores -", "disciplina!\n"};

    char sems_names[][25] = {"/sem_ex07_filho0", "/sem_ex07_filho1", "/sem_ex07_filho2"};
    sem_t* sem[N_SEMAPH];
    int sems_init_values[] = {1, 0, 0}; //estes valores permitem a sync entre os filhos
    int i;
    for(i = 0;i < N_SEMAPH;i++){
        if((sem[i] = sem_open(sems_names[i], O_CREAT | O_EXCL, 0644, sems_init_values[i]))== SEM_FAILED){
            perror("sem_open fails!\n");
            exit(EXIT_FAILURE);
        }
    }

    for(i = 0;i < CHILDS;i++){
        pid_t pid = fork();

        int j;
        if(pid == 0 && i == 0){ //child 0

            for(j = 0;j < 2;j++){
                sem_wait(sem[0]);
                puts(msgChild0[j]);
                sem_post(sem[1]);
            }
            exit(EXIT_SUCCESS);
        }

        if(pid == 0 && i == 1){ //child 1
            for(j = 0;j < 2;j++){
                sem_wait(sem[1]);
                puts(msgChild1[j]);
                sem_post(sem[2]);
            }
            exit(EXIT_SUCCESS);
        }

        if(pid == 0 && i == 2){ //child 2

            for(j = 0;j < 2;j++){
                sem_wait(sem[2]);
                puts(msgChild2[j]);
                sem_post(sem[0]);
            }
            exit(EXIT_SUCCESS);
        }
    }

    for(i = 0;i < 3;i++){
        wait(NULL);
    }

    for(i = 0;i < N_SEMAPH;i++){
        if(sem_close(sem[i])==-1){
            printf("sem_close failed: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }
    
    for(i = 0;i < N_SEMAPH;i++){
        if(sem_unlink(sems_names[i])==-1){
            printf("sem_unlink failed: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
