#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <pthread.h>
#include "ex11.h"

#define N_THREADS 5 //!5 threads
#define N_PROVAS 300
#define N_NOTAS 3
#define NOTA_POSITIVA >=50
#define NOTA_NEGATIVA <50
#define MIN_NOTA 0
#define MAX_NOTA 100
#define N_COND 3

Prova ArrayProva[300];
pthread_mutex_t mutexArrayProva;
pthread_mutex_t mutexPositivas;
pthread_mutex_t mutexNegativas;
pthread_mutex_t mutexIncrementar;
pthread_cond_t condArray[N_COND];
int lidos=0, escritos=0, nElements=0, nElementosPos=0, nElementosNeg=0, positivas=0, negativas=0, provasIncrementadas=0;

void* gerarProvas(void* dados){
    int i=0;
    printf("Thread 1 a gerar provas\n");
    for (i = 0; i < N_PROVAS; i++) {
        Prova p;
        p.number = i;
        p.notaG1 = rand () % (MAX_NOTA + 1 - MIN_NOTA) + MIN_NOTA;
        p.notaG2 = rand () % (MAX_NOTA + 1 - MIN_NOTA) + MIN_NOTA;
        p.notaG3 = rand () % (MAX_NOTA + 1 - MIN_NOTA) + MIN_NOTA;
        
        pthread_mutex_lock(&mutexArrayProva); //bloqueia para manipular vetor
        ArrayProva[i]=p;
        escritos++;
        nElements++;
        if(nElements>0){
            pthread_cond_broadcast(&condArray[0]);
        }
        pthread_mutex_unlock(&mutexArrayProva);
    }
    printf("Thread 1 a terminar\n");
    pthread_exit(NULL);
}

/*THREAD T2 T3*/
void* calcular(void *dados){
    int leituraConcluida = 0;
    int* threadNumber = (int*) dados;
    printf("Thread %d a calcular\n", *threadNumber);
    while(!leituraConcluida){
        pthread_mutex_lock(&mutexArrayProva);
        
        while(nElements==0 && lidos<N_PROVAS){ //vai verificando se existem provas para ler ou se já foram todas lidas
            pthread_cond_wait(&condArray[0], &mutexArrayProva);
        }

        if (lidos==N_PROVAS) { //verifica se ja foram lidas todas as provas
            leituraConcluida=1;
        } else {
            ArrayProva[lidos].notaFinal=(ArrayProva[lidos].notaG1+ArrayProva[lidos].notaG2+ArrayProva[lidos].notaG3) / N_NOTAS;
            
            pthread_mutex_lock(&mutexIncrementar);

            if(ArrayProva[lidos].notaFinal NOTA_POSITIVA){
                nElementosPos++;
                if(nElementosPos>0){
                    pthread_cond_signal(&condArray[2]);
                }
            } else {
                nElementosNeg++;
                if(nElementosNeg>0){
                    pthread_cond_signal(&condArray[3]);
                }
            }
            pthread_mutex_unlock(&mutexIncrementar);
            lidos++;
            nElements--;
        }
        pthread_mutex_unlock(&mutexArrayProva);
    }
    printf("Thread %d terminar\n", *threadNumber);
    pthread_exit(NULL);
}

void* incrementarPositivas(void* dados){
    printf("Thread 4 comecar\n");
    int existemProvasIncrementar=1;
    while(existemProvasIncrementar){

        pthread_mutex_lock(&mutexIncrementar);

        while(nElementosPos==0 && provasIncrementadas<N_PROVAS){
            printf("Preso no while de positivos\tprovas incrementadas=%d\n", provasIncrementadas);
            pthread_cond_wait(&condArray[2], &mutexIncrementar);
        }

        if (provasIncrementadas==N_PROVAS) {
            existemProvasIncrementar=0;
        } else{
            printf("positivas++\n");
            positivas++;
            nElementosPos--; 
            provasIncrementadas++;
        }
        pthread_mutex_unlock(&mutexIncrementar);
    }
    printf("Thread 4 terminar\n");
    pthread_exit(NULL);
}

void* incrementarNegativas(void* dados){
    printf("Thread 5 comecar\n");
    int existemProvasIncrementar=1;
    while(existemProvasIncrementar){

        pthread_mutex_lock(&mutexIncrementar);

        while(nElementosNeg==0 && provasIncrementadas<N_PROVAS){
            printf("Preso no while de negativos\tprovas incrementadas=%d\n", provasIncrementadas);
            pthread_cond_wait(&condArray[3], &mutexIncrementar);
        }
        
        if (provasIncrementadas==N_PROVAS) {
            existemProvasIncrementar=0;
        } else {
            printf("negativas++\n");
            negativas++;
            nElementosNeg--;
            provasIncrementadas++;
        }

        pthread_mutex_unlock(&mutexIncrementar);
        
    }
    printf("Thread 5 terminar\n");
    pthread_exit(NULL);
}

int main(){

    int i;

    srand(time(NULL));

    pthread_t threads[N_THREADS];

    if (pthread_mutex_init(&mutexArrayProva, NULL)) { //inicialiar mutex
        printf("pthread_mutex_init fails\n");
    }

    if (pthread_mutex_init(&mutexPositivas, NULL)) { //inicialiar mutex
        printf("pthread_mutex_init fails\n");
    }

    if (pthread_mutex_init(&mutexNegativas, NULL)) { //inicialiar mutex
        printf("pthread_mutex_init fails\n");
    }
    
    for (i = 0; i < N_COND; i++) { //inicializar variaveis de condicao
        
        if (pthread_cond_init(&condArray[i], NULL)) {
            printf("pthread_cond_init %d fails!\n", i);
        }
        
    }
    
    //Criacao das threads
    if (pthread_create(&threads[0], NULL, gerarProvas, 0)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }
    printf("Thread 1 criada\n");

    int num2 =2 ;
    if (pthread_create(&threads[1], NULL, calcular, (void*)&num2)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }
    printf("Thread 2 criada\n");

    int num3 =3 ;
    if (pthread_create(&threads[2], NULL, calcular, (void*)&num3)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }
    printf("Thread 3 criada\n");

    if (pthread_create(&threads[3], NULL, incrementarPositivas, 0)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }
    printf("Thread 4 criada\n");

    if (pthread_create(&threads[4], NULL, incrementarNegativas, 0)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }
    printf("Thread 5 criada\n");

    //Esperar pela threads
    for (i = 0; i < N_THREADS; i++) {
        if(pthread_join(threads[i], NULL)){
            printf("Join Thread %d fails!\n", i);
        }
    }

    printf("%d Negativas %.2f%%\n", negativas, ((float)negativas*100/N_PROVAS));
    printf("%d Positivas %.2f%%\n", positivas, ((float)positivas*100/N_PROVAS));

    //terminar variaveis de condicao
    for (i = 0; i < N_COND; i++) {  
        if (pthread_cond_destroy(&condArray[i])) {
            printf("pthread_cond_destroy %d fails \n", i);
        }
    }

    //terminar variaveis mutex
    if(pthread_mutex_destroy(&mutexArrayProva)){
        printf("pthread_mutex_destroy mutexArrayProva fails!\n");
    }
    if(pthread_mutex_destroy(&mutexPositivas)){
        printf("pthread_mutex_destroy mutexPositivas fails!\n");
    }
    if(pthread_mutex_destroy(&mutexNegativas)){
        printf("pthread_mutex_destroy mutexNegativas fails!\n");
    }

    return 0;
}


