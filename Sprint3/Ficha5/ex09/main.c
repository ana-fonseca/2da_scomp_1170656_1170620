#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define N_TRAINS 10
#define N_MUTEX 3
#define N_VIAGENS 12
#define N_CITYS 4

/**
 * pos 0 mutex caminho A-B ou B-A
 * pos 1 mutex caminho B-C ou C-B
 * pos 2 mutex caminho B-D ou D-B
 */
pthread_mutex_t mutex[N_MUTEX];

void create_threads(pthread_t *threads, int n_threads, void* (*function)(void*), int* func_args) {
	
	int i;
    printf("n_threads = %d\n", n_threads);
    for (i = 0; i < n_threads; i++) {
        if (pthread_create(&threads[i], NULL, function, (void*) &func_args[i])) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
        }
        printf("Created thread %d with ID = %lu\n", i, threads[i]);
    }
}

void viagem1(int trainNumber){ //A B
    
    int tempoViagem = 1;
    printf("Train %d\tViagem A->B\n", trainNumber);
    pthread_mutex_lock(&mutex[0]);
    sleep(tempoViagem);
    pthread_mutex_unlock(&mutex[0]);
    printf("Train %d\tA->B Duracao da viagem: %d h\n", trainNumber, tempoViagem);
}

void viagem2(int trainNumber){ //A D
    
    int tempoViagemAB = 1;
    int tempoViagemBD = 1;
    //A-B
    printf("Train %d\tViagem A->B\n", trainNumber);
    pthread_mutex_lock(&mutex[0]);
    sleep(tempoViagemAB);
    pthread_mutex_unlock(&mutex[0]);
    //B-D
    printf("Train %d\tViagem B->D\n", trainNumber);
    pthread_mutex_lock(&mutex[2]);
    sleep(tempoViagemBD);
    pthread_mutex_unlock(&mutex[2]);

    printf("Train %d\tA->D Duracao da viagem: %d h\n", trainNumber, tempoViagemAB+tempoViagemBD);
}

void viagem3(int trainNumber){ //C D
    
    int tempoViagemCB = 1;
    int tempoViagemBD = 1;
    //A-B
    printf("Train %d\tViagem C->B\n", trainNumber);
    pthread_mutex_lock(&mutex[1]);
    sleep(tempoViagemCB);
    pthread_mutex_unlock(&mutex[1]);
    //B-D
    printf("Train %d\tViagem B->D\n", trainNumber);
    pthread_mutex_lock(&mutex[2]);
    sleep(tempoViagemBD);
    pthread_mutex_unlock(&mutex[2]);

    printf("Train %d\tC->D Duracao da viagem: %d h\n", trainNumber, tempoViagemCB+tempoViagemBD);
}

void viagem4(int trainNumber){ //D A
    
    int tempoViagemDB = 1;
    int tempoViagemBA = 1;
    //A-B
    printf("Train %d\tViagem D->B\n", trainNumber);
    pthread_mutex_lock(&mutex[2]);
    sleep(tempoViagemDB);
    pthread_mutex_unlock(&mutex[2]);
    //B-D
    printf("Train %d\tViagem B->A\n", trainNumber);
    pthread_mutex_lock(&mutex[0]);
    sleep(tempoViagemBA);
    pthread_mutex_unlock(&mutex[0]);

    printf("Train %d\tD->A Duracao da viagem: %d h\n", trainNumber, tempoViagemDB+tempoViagemBA);
}

void viagem5(int trainNumber){ //B C
    
    int tempoViagem = 1;
    printf("Train %d\tViagem B->C\n", trainNumber);
    pthread_mutex_lock(&mutex[1]);
    sleep(tempoViagem);
    pthread_mutex_unlock(&mutex[1]);
    printf("Train %d\tB->C Duracao da viagem: %d h\n", trainNumber, tempoViagem);
}

//void criarItenerarios

void* train(void* dados){

    int viagem = rand () % (5) + 1; //de 1 a 5
    int* trainNumber = (int*)dados;
    int num = *trainNumber;
    printf("Train %d vai fazer a viagem %d\n", num, viagem);
    
    if (viagem==1) {
        viagem1(*trainNumber);
    } else if (viagem==2) {
        viagem2(*trainNumber);
    } else if (viagem==3) {
        viagem3(*trainNumber);
    } else if (viagem==4) {
        viagem4(*trainNumber);
    } else if (viagem==5) {
        viagem4(*trainNumber);
    }

    pthread_exit(NULL);
}

int main(){
    srand(time(NULL));

    int i = 0;

    pthread_mutex_t mutex[N_MUTEX];

    //iniciar mutex
    for (i = 0; i < N_MUTEX; i++) {
        if(pthread_mutex_init(&mutex[i], NULL)){
            printf("Error init mutex %d\n", i);
        }  
    }

    pthread_t threads[N_TRAINS];
    int num[N_TRAINS];

    for (i = 0; i < N_TRAINS; i++) {
        num[i]=i;
    }

    create_threads(threads, N_TRAINS, train, &num[0]);
    
    //esperar pelas threads
    for (i = 0; i < N_TRAINS; i++) {
        if(pthread_join(threads[i], NULL)){
            printf("Join Thread %d fails!\n", i);
        }
    }
    
    return 0;
}


