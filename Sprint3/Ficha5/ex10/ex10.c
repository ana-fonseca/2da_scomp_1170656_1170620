#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <pthread.h>

#define MAX_INFORMATION 10000
#define NUM_THREADS 6
#define MAX_PRODUCTS 5
#define NUM_MARKETS 3
#define MUTEX 3

typedef struct{
	int id_h;
	int id_p;
	float p;
}TripleData;

typedef struct{
	TripleData vec[MAX_INFORMATION];
	TripleData vec1[MAX_INFORMATION];
	int h1; //Hipermercado1
	TripleData vec2[MAX_INFORMATION]; 
	int h2; //Hipermercado2
	TripleData vec3[MAX_INFORMATION]; 
	int h3; //Hipermercado3
	int mutex;
}Markets;

pthread_t threads[NUM_THREADS];
pthread_mutex_t mutexs[MUTEX];
int lower_price, h_id;

void* filtering(void* arg){
	
	Markets* m = (Markets*) arg;
	int a;
	pthread_mutex_lock(&mutexs[MUTEX/2]);
	int mutex = m->mutex;
	m->mutex++;
	pthread_mutex_unlock(&mutexs[MUTEX/2]);
	
	for(a = MAX_INFORMATION / (MUTEX/2) * mutex; a < MAX_INFORMATION / (MUTEX/2) * (mutex + 1); a++){
		if(m->vec[a].id_h == 1){
			pthread_mutex_lock(&mutexs[0]);
				m->vec1[m->h1] = m->vec[a];
				m->h1++;
			pthread_mutex_unlock(&mutexs[0]);
		} else if(m->vec[a].id_h == 2){
			pthread_mutex_lock(&mutexs[1]);
				m->vec2[m->h2] = m->vec[a];
				m->h2++;
			pthread_mutex_unlock(&mutexs[1]);
		} else{
			pthread_mutex_lock(&mutexs[2]);
				m->vec3[m->h3] = m->vec[a];
				m->h3++;
			pthread_mutex_unlock(&mutexs[2]);
		}
	}
	
	pthread_exit((void*) NULL);
	
}

void* computing(void* arg){
	
	Markets* m = (Markets*) arg;
	int i, cost = 0;
	
	pthread_mutex_lock(&mutexs[MUTEX/2]);
	
		int mutex = m->mutex;
		m->mutex++;
	
	pthread_mutex_unlock(&mutexs[MUTEX/2]);
	
	if(mutex == 0){
		for(i = 0; i < m->h1; i++)
			cost += m->vec1[i].p;
		
		lower_price = cost / m->h1 * MAX_PRODUCTS;
		h_id = 1;
	} else if(mutex == 1){
		for(i = 0; i < m->h2; i++)
			cost += m->vec2[i].p;
		
		if(lower_price > cost / m->h2 * MAX_PRODUCTS){
			lower_price = cost / m->h2 * MAX_PRODUCTS;
			h_id = 2;
		}
	} else{
		for(i = 0; i < m->h3; i++)
			cost += m->vec3[i].p;
		
		if(lower_price > cost / m->h3 * MAX_PRODUCTS){
			lower_price = cost / m->h3 * MAX_PRODUCTS;
			h_id = 3;
		}
	}
	
	
	pthread_exit((void*) NULL);
}

int main(){
	
	Markets* m = malloc(sizeof(Markets));
	
	srand((unsigned) time(NULL));
	int i;
	m->mutex = 0; m->h1 = 0; m->h2 = 0; m->h3 = 0;
	//Fill vec randomly with correct information
	for(i = 0; i < MAX_INFORMATION; i++){
		m->vec[i].id_h = (rand() % NUM_MARKETS) + 1;
		m->vec[i].id_p = (rand() % MAX_INFORMATION) + 1;
		m->vec[i].p = (rand() % 10) + 1;
	}
	
	
	for(i = 0; i < MUTEX; i++){
		if(pthread_mutex_init(&mutexs[i], NULL) != 0){
			perror("Error on pthread_mutex_init\n");
			exit(EXIT_FAILURE);
		}
	}
	
    for(i=0; i < MUTEX/2; i++){
		if(pthread_create(&threads[i], NULL, filtering, (void*) m) != 0){
				perror("Error on pthread_create\n");
				exit(EXIT_FAILURE);
		}
	}
	
	
	for(i=0; i < MUTEX/2; i++){
		if(pthread_join(threads[i], NULL) != 0){
			perror("Error on pthread_join\n");
			exit(EXIT_FAILURE);
		}
    }
    
    m->mutex = 0;
    
    
	for(i=MUTEX/2; i < MUTEX; i++){
		if(pthread_create(&threads[i], NULL, computing, (void*) m) != 0){
				perror("Error on pthread_create\n");
				exit(EXIT_FAILURE);
		}
	}
	
	for(i=MUTEX/2; i < MUTEX; i++){
		if(pthread_join(threads[i], NULL) != 0){
			perror("Error on pthread_join\n");
			exit(EXIT_FAILURE);
		}
    }
	
	printf("Hypermarket Id: %d\nCost: %d\n", h_id, lower_price);
	
	for(i = 0; i < 4; i++){
		if(pthread_mutex_destroy(&mutexs[i]) != 0){
			perror("Error on pthread_mutex_destroy\n");
			exit(EXIT_FAILURE);
		}
	}
	
	free(m);

return 0;
}
