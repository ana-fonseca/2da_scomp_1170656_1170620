#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <pthread.h>

#define NUM_MATRICES 2
#define MSIZE 5
#define NUM_THREADS 7 //2 to fill + 5 to calculate
#define FILLING_THREADS 2

typedef struct{
    int matrix1[MSIZE][MSIZE];
	int matrix2[MSIZE][MSIZE];
	int result[MSIZE][MSIZE];
	int a;
} Matrices;

void* fill_matrix(void* arg, void* num){
	
	Matrices* m = (Matrices*) arg;
	int x, y;	
	
	srand((unsigned) time(NULL));
	
	for(x = 0; x < MSIZE; x++){
		for(y = 0; y < MSIZE; y++){
			
			if(m->a == 1){
				int val = rand() % 5;
				m->matrix1[x][y] = val;
			}else{
				int val = rand() % 5 + 1;
				m->matrix2[x][y] = val;
				
			}
		}
	}
	
	pthread_exit((void*) NULL);
}

void* calculations(void* arg){
	Matrices* m = (Matrices*) arg;
	int y;
	
	for(y = 0; y < MSIZE; y++)
		m->result[m->a][y] += m->matrix1[m->a][y] * m->matrix2[m->a][y];
	
	
	pthread_exit((void*) NULL);
}



int main(){
	Matrices* m = malloc(sizeof(Matrices));
    pthread_t threads[NUM_THREADS];
    int i,x,z; 
    m->a = 1;
    
    for(i = 0; i < FILLING_THREADS; i++){
		if(pthread_create(&threads[i], NULL, fill_matrix, (void*) m) != 0){
			perror("Error on pthread_create\n");
			exit(EXIT_FAILURE);
		}
		
		if(pthread_join(threads[i], NULL) != 0){
			perror("Error on pthread_join\n");
			exit(EXIT_FAILURE);
		}
		
		m->a++;
		
	}
	
	m->a = 0;
	
	for(i = 0; i < NUM_THREADS - FILLING_THREADS; i++){
		if(pthread_create(&threads[i+2], NULL, calculations, (void*) m) != 0){
			perror("Error on pthread_create\n");
			exit(EXIT_FAILURE);
		}
		
		if(pthread_join(threads[i], NULL) != 0){
			perror("Error on pthread_join\n");
			exit(EXIT_FAILURE);
		}
		
		m->a++;
		
	}
	
		printf("1:\n");
	for(z = 0; z < MSIZE; z++){
		for(x = 0; x < MSIZE; x++)
			printf("%d ", m->matrix1[z][x]);
	printf("\n");	
	}
	printf("2:\n");
	for(z = 0; z < MSIZE; z++){
		for(x = 0; x < MSIZE; x++)
			printf("%d ", m->matrix2[z][x]);
	printf("\n");	
	}
	printf("Res:\n");
	for(z = 0; z < MSIZE; z++){
		for(x = 0; x < MSIZE; x++)
			printf("%d ", m->result[z][x]);
	printf("\n");	
	}
			
	free(m);
return 0;
}
