#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <pthread.h>

#define SIZE 1000
#define NUM_THREADS 5
#define MUTEX 6

typedef struct{
	int data[SIZE];
	int result[SIZE];
	int mutex;
}Data;

pthread_t threads[NUM_THREADS];
pthread_mutex_t mutexs[MUTEX];

int calculateResult (int num){
	return num * 2 + 10;
}

void* thread_func(void* arg){
	Data* d = (Data*) arg;
	int i;
	
	pthread_mutex_lock(&mutexs[5]);
	
		int mutex = d->mutex;
		d->mutex++;
		
	pthread_mutex_unlock(&mutexs[5]);
	
	for(i = SIZE / NUM_THREADS * mutex; i < SIZE / NUM_THREADS * (mutex + 1); i++)
		d->result[i] = calculateResult(d->data[i]);
	
	pthread_mutex_lock(&mutexs[mutex % NUM_THREADS]);
	
		for(i = SIZE / NUM_THREADS * mutex; i < SIZE / NUM_THREADS * (mutex + 1); i++)
			printf("matrix[%d] = %d \n",i,  d->result[i]);
	
	pthread_mutex_unlock(&mutexs[(mutex + 1) % NUM_THREADS]);
	if(mutex > 0)
		pthread_mutex_unlock(&mutexs[mutex % NUM_THREADS]);
	
	pthread_exit((void*) NULL);
}

int main(){
	Data* d = malloc(sizeof(Data));
	d->mutex = 0;
	srand((unsigned) time(NULL));
	
	int i;
	//Fill in data array randomly
	for(i = 0; i < SIZE; i++){
		d->data[i] = rand() % 3;
	}
	
	for(i = 0; i < MUTEX; i++){
		if(pthread_mutex_init(&mutexs[i], NULL) != 0){
			perror("Error on pthread_mutex_init\n");
			exit(EXIT_FAILURE);
		}
		
		if(i > 0 && i < 5)
			pthread_mutex_lock(&mutexs[i]);
	}
	
    for(i=0; i < MUTEX; i++){
		if(pthread_create(&threads[i], NULL, thread_func, (void*) d) != 0){
				perror("Error on pthread_create\n");
				exit(EXIT_FAILURE);
		}
    }
	
	for(i=0; i < MUTEX; i++){
		if(pthread_join(threads[i], NULL) != 0){
			perror("Error on pthread_join\n");
			exit(EXIT_FAILURE);
		}
    }
	
	
	printf("\n");
	fflush(0);
	
	for(i = 0; i < MUTEX; i++){
		if(pthread_mutex_destroy(&mutexs[i]) != 0){
			perror("Error on pthread_mutex_destroy\n");
			exit(EXIT_FAILURE);
		}
	}

return 0;
}
