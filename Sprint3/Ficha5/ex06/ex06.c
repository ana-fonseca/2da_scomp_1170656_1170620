#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <pthread.h>

int isPrime(int number){
	int i;
	for(i = number - 1; i > 1; i--){
		if(number % i == 0)
			return 0;
	}
	return 1;
}

void* validate(void* arg){
	int number = *(int*) arg, i;
	
	printf("Prime Numbers: ");
	
	for(i = number; i > 0; i--){
		if(isPrime(i) == 1)
			printf("%d ", i);
	}
	printf("\n");
	
	pthread_exit((void*) NULL);
}

int main(){
pthread_t thread;
	int number;

	printf("Insert a limit number: \n");
	scanf("%d", &number);

	if(pthread_create(&thread, NULL, validate, (void*)&number) != 0){
		perror("Error on pthread_create\n");
		exit(EXIT_FAILURE);
	}
	
	if(pthread_join(thread, NULL) != 0){
		perror("Error on pthread_join\n");
		exit(EXIT_FAILURE);
	}

return 0;
}
