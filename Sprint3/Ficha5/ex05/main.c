#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define N_THREADS 3
#define MAIN_ARRAY_SIZE 1000
#define MAX_RANGE 2000
#define MIN_RANGE 0

//global variables
int lowestBalance = 0;
int highestBalance = 0;
int averageBalance = 0;

void* searchLowestBalance(void* dados){
    
    int* array = (int*) dados;
    int i = 0, lowestValue=MAX_RANGE;
    
    for (i = 0; i < MAIN_ARRAY_SIZE; i++) {
        if(array[i]<lowestValue){
            lowestValue=array[i];
        }
    }
    lowestBalance = lowestValue;
    pthread_exit(NULL);
}

void* searchHighestBalance(void* dados){

    int* array = (int*) dados;
    int i = 0, highestValue=MIN_RANGE;
    for (i = 0; i < MAIN_ARRAY_SIZE; i++) {
        if(array[i]>highestValue){
            highestValue=array[i];
        }
    }
    highestBalance = highestValue;
    pthread_exit(NULL);
}

void* calculateAverageBalance(void* dados){

    int* array = (int*) dados;
    int i = 0, sum=0;
    for (i = 0; i < MAIN_ARRAY_SIZE; i++) {
        sum+=array[i];
    }
    averageBalance = sum/MAIN_ARRAY_SIZE;
    pthread_exit(NULL);
}

int main(){

    int mainArray[MAIN_ARRAY_SIZE];
    int i;

    srand(time(NULL));

    for (i = 0; i < MAIN_ARRAY_SIZE; i++) {
        mainArray[i]=rand () % (MAX_RANGE + 1 - MIN_RANGE) + MIN_RANGE;
    }

    pthread_t threads[N_THREADS];

    //Criacao das threads
    //lowest
    if (pthread_create(&threads[0], NULL, searchLowestBalance, (void*) &mainArray)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }

	//highest
    if (pthread_create(&threads[1], NULL, searchHighestBalance, (void*) &mainArray)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }

	//average
    if (pthread_create(&threads[2], NULL, calculateAverageBalance, (void*) &mainArray)) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
    }
    //Fim da criacao das threads
    
    //wait pelo fim das threads
    for (i = 0; i < N_THREADS; i++) {
        if(pthread_join(threads[i], NULL)){
            printf("Join Thread %d fails!\n", i);
        }
    }

    printf("Lowest balance = %d\n", lowestBalance);
    printf("Highest balance = %d\n", highestBalance);
    printf("Average balance = %d\n", averageBalance);

    return EXIT_SUCCESS;
}
