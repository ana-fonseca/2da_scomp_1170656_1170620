#define SEARCH_ARRAY_SIZE 1000
#define NUM_NUMBERS 49 
#define KEYS 5
#include <pthread.h>

typedef struct {
	int threadNumber;
	pthread_mutex_t *mutex[NUM_NUMBERS];
	int *array[SEARCH_ARRAY_SIZE][KEYS];
} data;
