#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include "ex07.h"

#define KEYS_NUMBER 10000
#define NUM_NUMBERS 49
#define SET_NUMBER 5
#define N_THREADS 10
#define MIN_DRAW_EXCL 1
#define MAX_DRAW_EXCL 49

int occurrenceArray[NUM_NUMBERS];

void create_threads(pthread_t *threads, int n_threads, void* (*function)(void*), data* func_args) {
	
    printf("A criar threads...\n");
	int i;
    for (i = 0; i < n_threads; i++) {
        if (pthread_create(&threads[i], NULL, function, (void*) &func_args[i])) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
        }
        printf("Created thread %d with ID = %lu\n", i+1, threads[i]);
    }
}

void* count(void* dados){

    data* search = (data*) dados;

    int i = 0;

    int *arraySearch = search->array[0][0];
    pthread_mutex_t *mutexArray = search->mutex[0]; //um array com um para cada
    
    for (i = 0; i < SEARCH_ARRAY_SIZE; i++) {
        int j;
        for (j = 0; j < SET_NUMBER; j++) {
			//para cada numero que encontra faz lock ao seu mutex associado soma +1 e da unlock
            int num = *(arraySearch+i*SET_NUMBER+j);
            int pos = num-1; //posicao corresponde ao numero-1
            
            pthread_mutex_lock(&mutexArray[pos]);
            occurrenceArray[pos]+=1;
            pthread_mutex_unlock(&mutexArray[pos]);
        }
    }
    printf("Thread %d calculos finalizados!\n", search->threadNumber);
    pthread_exit(NULL);
}


int main(){
    srand(time(NULL));
    int i = 0;

    pthread_mutex_t mutex[NUM_NUMBERS];

    for (i = 0; i < NUM_NUMBERS; i++) {
        if(pthread_mutex_init(&mutex[i], NULL)){
            printf("Error init mutex %d\n", i);
        }    
    }

    int keysArray[KEYS_NUMBER][SET_NUMBER];

    //inicializar vetor de ocorrencias
    for (i = 0; i < NUM_NUMBERS; i++) {
        occurrenceArray[i]=0;
    }

    //preencher vetor de keys com valores sorteados
    for (i = 0; i < KEYS_NUMBER; i++) {
        int j=0;
        for (j = 0; j < SET_NUMBER; j++) {
            keysArray[i][j] = rand () % (MAX_DRAW_EXCL + 1 - MIN_DRAW_EXCL) + MIN_DRAW_EXCL;
            printf("%d\t", keysArray[i][j]);
        }
        printf("\n");
    }

    pthread_t threads[N_THREADS];
    data dados[N_THREADS];

    for (i = 0; i < N_THREADS; i++) {
        dados[i].array[0][0]=(&keysArray[0][0])+i*SEARCH_ARRAY_SIZE*SET_NUMBER;
        dados[i].mutex[0]=&mutex[0];
        dados[i].threadNumber=i+1;
    }
    
    create_threads(threads, N_THREADS, count, dados);

	//esperar pelas threads acabarem todas
    for (i = 0; i < N_THREADS; i++) {
        if(pthread_join(threads[i], NULL)){
            printf("Join Thread %d fails!\n", i);
        }
    }

    for (i = 0; i < NUM_NUMBERS; i++) {
        pthread_mutex_destroy(&mutex[i]);
    }

    for (i = 0; i < NUM_NUMBERS; i++) {
        printf("Numero %d saiu %dx\n", i+1,occurrenceArray[i]);
    }

    return 0;
}
