#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include "ex03.h"

#define N_THREADS 5
#define MAIN_ARRAY_SIZE 1000

int threadNumber = -1;

void create_threads(pthread_t *threads, int n_threads, void* (*function)(void*), data* func_args) {
	
	int i;
    for (i = 0; i < n_threads; i++) {
        if (pthread_create(&threads[i], NULL, function, (void*) &func_args[i])) {
            perror("pthread_create failed :/");
            exit(EXIT_FAILURE);
        }
        printf("Created thread %d with ID = %lu\n", i, threads[i]);
    }
}

void* search_number(void* dados){
    data* toSearch = (data*) dados;

    int i = 0;
    printf("Thread %d comecou a procura do numero %d\n", toSearch->threadNumber, toSearch->numberToSearch);
    int* arraySearch =toSearch->array[i];
    for (i = 0; i < ARRAY_SIZE; i++) {
        if(arraySearch[i]==toSearch->numberToSearch){
            printf("Numero encontrado na posicao: %d\n\n",toSearch->threadNumber*ARRAY_SIZE+i);

            pthread_exit((void*)toSearch->threadNumber);
        }
    }
    printf("\n");
    pthread_exit(NULL);
}

int main(){

    int mainArray[MAIN_ARRAY_SIZE];
    int i;

	//numero unico em cada (index)
    for (i = 0; i < MAIN_ARRAY_SIZE; i++) {
        mainArray[i]=i;
    }

    pthread_t threads[N_THREADS];
    //struct no .h
    data dados[N_THREADS];

    //numero para procurar
    srand (time (NULL)*getpid());
    int numberToSearch = rand () % (1000);

    printf("Numero para procurar: %d\n", numberToSearch);
    
    //atribuir os dados que cada thread vai usar 
    for (i = 0; i < N_THREADS; i++) {
		dados[i].array[0]=(&mainArray[0])+i*ARRAY_SIZE;
        dados[i].numberToSearch=numberToSearch;
        dados[i].threadNumber=i+1;
	}

	create_threads(threads, N_THREADS, search_number, dados);

    printf("Esperar pelas threads\n");
    for (i = 0; i < N_THREADS; i++) {
        int valor =-1;
        pthread_join(threads[i],(void*) &valor);
        //0 if not found, other the number of thread
        printf("Thread %d with ID %lu found? %d\n", i+1, threads[i], valor);
    }

    return EXIT_SUCCESS;
}
