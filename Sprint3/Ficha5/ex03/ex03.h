#define ARRAY_SIZE 200

typedef struct {
	int threadNumber;
	int numberToSearch;
	int *array[ARRAY_SIZE];
} data;
