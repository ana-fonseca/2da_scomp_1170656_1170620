#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h> 
#include <time.h>
#include <pthread.h>

#define SIZE 30
#define NUM_THREADS 5
#define NAME "ExampleName"
#define ADDRESS "ExampleAddress"

typedef struct{
	int number;
	char name[SIZE];
	char address[SIZE];
} Data;

void* func(void* arg){
		Data* d = (Data*) arg;
		printf("NUMBER: %d\nNAME: %s\nADDRESS: %s\n\n", d->number + 1, d->name, d->address);
		pthread_exit((void*) NULL);
}

int main(){

	Data* arrayStruct[NUM_THREADS];
	
	// Fill array of Data Structures 
	int i;
	for (i = 0; i < NUM_THREADS; i++){
		Data* d = malloc(sizeof(Data));
		d->number = i;
		strcpy(d->name, NAME);
		strcpy(d->address, ADDRESS);	
		arrayStruct[i] = d;	//add to the array
	}
	
	pthread_t threads[NUM_THREADS];
	
	for(i=0; i<NUM_THREADS; i++){
		
		if(pthread_create(&threads[i], NULL, func, (void*) arrayStruct[i]) != 0){
			perror("Error creating thread!\n");
			exit(EXIT_FAILURE);
		}
		
		//Garante que vão correr ordenadamente
		if(pthread_join(threads[i], NULL) != 0){
			perror("Error waiting for thread!\n");
			exit(EXIT_FAILURE);
		}
		
			
		free(arrayStruct[i]);
	}
	

return 0;
}
